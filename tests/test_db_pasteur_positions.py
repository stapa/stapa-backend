import pytest
from uuid import uuid4
from datetime import datetime, timedelta

from sqlalchemy import select, create_engine
from sqlalchemy.orm import Session
from sqlalchemy.exc import MultipleResultsFound


from stapa.backend.db.database import Base
from stapa.backend.db.models import User, PasteurPosition


@pytest.fixture
def engine(scope='session'):
    #engine = create_engine("sqlite:////tmp/test.db")
    engine = create_engine("postgresql+psycopg2://stapa_tests:stapa_tests@localhost/stapa_test")
    Base.metadata.create_all(engine)
    yield engine
    Base.metadata.drop_all(engine)
    engine.dispose()


def test_create_schema(engine):
    pass


def test_add_position(engine):
    with Session(engine) as session:
        session.add(PasteurPosition(description="PhD Student"))
        session.commit()


def test_retrieve_position(engine):
    with Session(engine) as session:
        pos1 = PasteurPosition(description="Position 1")
        pos2 = PasteurPosition(description="Position 2")
        session.add(pos1)
        session.add(pos2)
        session.commit()
        assert pos1.id == 1
        assert pos2.id == 2

    with Session(engine) as session:
        pos1q = session.execute(select(PasteurPosition).where(PasteurPosition.id == 1)).scalar_one_or_none()
        assert pos1q.description == "Position 1"    
        pos2q = session.execute(select(PasteurPosition).where(PasteurPosition.id == 2)).scalar_one_or_none()
        assert pos2q.description == "Position 2"    
    

def create_user():
    return User(
        given_name="Test",
        family_name="User",
        display_name="Test User",
        pasteur_azure_ad_oid=uuid4()
    )

def test_create_user_without_position(engine):
    with Session(engine) as session:
        user = create_user()
        session.add(user)
        session.commit()
        assert user.id == 1

def test_create_user_with_position(engine):
    with Session(engine) as session:
        pos = PasteurPosition(description="Test Position")
        session.add(pos)
        session.commit()

    with Session(engine) as session:
        user = create_user()
        user.pasteur_position_id = 1
        session.add(user)
        session.commit()
        assert user.id == 1
        assert user.pasteur_position_id == 1
        assert user.pasteur_position.id == 1
        assert user.pasteur_position.description == "Test Position"
        assert user.pasteur_position_other == None


def test_create_user_with_position_other(engine):
    with Session(engine) as session:
        user = create_user()
        user.pasteur_position_other = "IP President"
        session.add(user)
        session.commit()
        assert user.id == 1
        assert user.pasteur_position_id == None
        assert user.pasteur_position == None
        assert user.pasteur_position_other == "IP President"
