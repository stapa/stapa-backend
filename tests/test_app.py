import pytest
from sqlalchemy import create_engine

from stapa.backend.config import settings
settings.DB_URL = "postgresql+psycopg2://stapa_tests:stapa_tests@localhost/stapa_test"

import stapa.backend.app
from stapa.backend.db.database import Base


@pytest.fixture
def app():
    engine = create_engine(settings.DB_URL)
    Base.metadata.create_all(engine)
    engine.dispose()
    app = stapa.backend.app.app
    yield app
    engine = create_engine(settings.DB_URL)
    Base.metadata.drop_all(engine)
    engine.dispose()


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def auth_client(client, helpers):
    res = client.post(
        '/auth/login',
        json={'redirect_url': settings.AUTHORIZED_REDIRECTS[0]}
    )
    callback_url = helpers.perform_msal_login(
        res.json['login_url'],
        settings.TESTING_INTEGRATION_AZURE_USERNAME,
        settings.TESTING_INTEGRATION_AZURE_PASSWORD
    )
    res = client.get(callback_url)
    return client


def test_authenticated_client(auth_client):
    res = auth_client.post(
        '/auth/login',
        json={'redirect_url': settings.AUTHORIZED_REDIRECTS[0]}
    )
    assert res.json['description'] == "Already logged in."


def test_me_unauth(client):
    res = client.get('/me')
    assert res.status_code == 401


def test_me_auth(auth_client):
    res = auth_client.get('/me')
    assert res.status_code == 200
    assert res.json['oid']
    assert res.json['name']
    assert res.json['username']


def test_azure_sync_db(auth_client):
    res = auth_client.get('/onboarding/auth')
