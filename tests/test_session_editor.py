from datetime import timedelta
import pytest
import random

from flask.sessions import SecureCookieSessionInterface
from flask_session import Session


from stapa.backend.utils.session_editor import (
    UnsupportedSessionInterface,
    SessionNotFound,
    SessionEditor,
    FileSystemSessionInterface
)


def test_session_editor_securecookie():
    class App:
        pass
    app = App()
    app.config = {}
    app.session_interface = SecureCookieSessionInterface()
    with pytest.raises(UnsupportedSessionInterface):
        session_editor = SessionEditor.from_app(app)


@pytest.fixture
def session_editor(tmp_path):
    class App:
        pass
    app = App()
    app.permanent_session_lifetime = timedelta(days=31)
    app.config = {
        'SESSION_TYPE': 'filesystem',
        'SESSION_FILE_DIR': str(tmp_path)}
    Session(app)

    return SessionEditor.from_app(app)


@pytest.fixture
def random_tk_sid():
    unique_tk = {
        f'tk{random.randint(0,1000)}': f'sid{random.randint(0,1000)}'
        for i in range(0, 100)
    }
    return list(unique_tk.items())


@pytest.fixture
def ordered_tk_sid():
    unique_tk = {
        f'tk{i}': f'sid{i}'
        for i in range(0, 100)
    }
    return list(unique_tk.items())


def test_save_tokens(session_editor, random_tk_sid):
    for tk, sid in random_tk_sid:
        session_editor.save_token(tk, sid)


def test_load_token(session_editor, random_tk_sid):
    for tk, sid in random_tk_sid:
        session_editor.save_token(tk, sid)
    random.shuffle(random_tk_sid)
    for tk, sid in random_tk_sid:
        assert session_editor.load_token(tk) == sid


def test_clear_token(session_editor, random_tk_sid):
    for tk, sid in random_tk_sid:
        session_editor.save_token(tk, sid)
    random.shuffle(random_tk_sid)
    for tk, sid in random_tk_sid:
        session_editor.load_token(tk)
        assert session_editor.load_token(tk) is None


def test_read_session_empty(session_editor):
    assert session_editor.read_session('sid0') == {}


def test_save_sessions(session_editor, random_tk_sid):
    for tk, sid in random_tk_sid:
        session_editor.save_session(sid, {'tk': tk})


def test_read_sessions(session_editor, ordered_tk_sid):
    random.shuffle(ordered_tk_sid)
    for tk, sid in ordered_tk_sid:
        session_editor.save_session(sid, {'tk': tk})

    random.shuffle(ordered_tk_sid)
    for tk, sid in ordered_tk_sid:
        data = session_editor.read_session(sid)
        assert data['tk'] == tk


def test_session_dict_read(session_editor, ordered_tk_sid):
    random.shuffle(ordered_tk_sid)
    for tk, sid in ordered_tk_sid:
        session_editor.save_session(sid, {'tk': tk})

    random.shuffle(ordered_tk_sid)
    for tk, sid in ordered_tk_sid:
        assert session_editor.session(sid)['tk'] == tk


def test_session_dict_write(session_editor, ordered_tk_sid):
    random.shuffle(ordered_tk_sid)
    for tk, sid in ordered_tk_sid:
        session_editor.save_session(sid, {'tk': tk})

    random.shuffle(ordered_tk_sid)
    for tk, sid in ordered_tk_sid:
        session_editor.session(sid)['tksid'] = tk + sid

    random.shuffle(ordered_tk_sid)
    for tk, sid in ordered_tk_sid:
        assert session_editor.session(sid)['tksid'] == tk + sid


def test_session_dict_non_exists_key(session_editor, ordered_tk_sid):
    tk, sid = ordered_tk_sid[0]
    assert session_editor.session(sid).get('tk') is None
