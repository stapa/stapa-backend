from pathlib import Path
from secrets import token_urlsafe
import tempfile
import time
from urllib.parse import urlparse, parse_qs


import pytest
import requests
from sqlalchemy import create_engine

from stapa.backend.config import settings
temp_cache_dir = tempfile.TemporaryDirectory()
settings.DB_URL = "postgresql+psycopg2://stapa_tests:stapa_tests@localhost/stapa_test"
settings.SESSION_FILE_THRESHOLD = 10
settings.SESSION_TYPE = "filesystem"
settings.SESSION_FILE_DIR = temp_cache_dir.name

import stapa.backend.app
from stapa.backend.db.database import Base
from stapa.backend.auth.native import NativeToken


@pytest.fixture
def app():
    engine = create_engine(settings.DB_URL)
    Base.metadata.create_all(engine)
    engine.dispose()
    app = stapa.backend.app.app
    yield app
    engine = create_engine(settings.DB_URL)
    Base.metadata.drop_all(engine)
    engine.dispose()
    # empty cache folder
    for p in Path(temp_cache_dir.name).glob('*'):
        p.unlink()


@pytest.fixture
def app_with_full_cache(app):
    with tempfile.TemporaryDirectory() as tmpdir:
        with app.test_client(use_cookies=False) as client:
            for i in range(0, settings.SESSION_FILE_THRESHOLD):
                res = client.post('/auth/login', json={'redirect_url': 'https://test-authorized-redirect.com'})
        yield app


def test_native_token_with_cache_full(app_with_full_cache):
    client = app_with_full_cache.test_client(use_cookies=True)
    settings.AUTHORIZED_REDIRECTS = ['https://test-authorized-redirect.com']
    res = client.post(
        '/auth/login',
        json={'redirect_url': 'https://test-authorized-redirect.com', 'native_app': True}
    )
    state = parse_qs(urlparse(res.json['login_url']).query)['state'][0]
    native_token = NativeToken.from_state(app_with_full_cache, state)
    assert native_token.sid is not None
