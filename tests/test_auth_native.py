from datetime import timedelta
import pytest
import random

from flask_session import Session

from stapa.backend.auth.native import NativeToken, NativeTokenNotFound


@pytest.fixture
def mock_app(tmp_path):
    class App:
        pass
    app = App()
    app.config = {
        'SESSION_TYPE': 'filesystem',
        'SESSION_FILE_DIR': str(tmp_path)
    }
    app.session_cookie_name = "session_id"
    app.permanent_session_lifetime = timedelta(days=31)
    Session(app)
    return app


def test_native_token_create(mock_app):
    tk = NativeToken(mock_app, 'token0', sid='sid0')
    assert tk.token == 'token0'
    assert tk.sid == 'sid0'


def test_native_token_get(mock_app):
    tk = NativeToken(mock_app, 'token0', sid='sid0')
    read_tk = NativeToken(mock_app, 'token0')
    assert read_tk.sid == 'sid0'


@pytest.fixture
def sid_tokens(mock_app):
    class MockSession:
        pass

    tokens = []
    for i in range(0, 20):
        session = MockSession()
        session.sid = f'sid{i}'
        tokens.append((
            f'sid{i}',
            NativeToken.from_session(mock_app, session)
        ))
    return tokens


def test_native_token_from_session(mock_app, sid_tokens):
    random.shuffle(sid_tokens)
    for sid, tk in sid_tokens:
        assert tk.token
        assert tk.sid == sid


def test_native_token_to_state(mock_app, sid_tokens):
    random.shuffle(sid_tokens)
    for sid, tk in sid_tokens:
        state = tk.to_state()
        assert state == 'nativeToken:' + tk.token


def test_native_token_from_state(mock_app, sid_tokens):
    random.shuffle(sid_tokens)
    for sid, tk in sid_tokens:
        state = tk.to_state()
        loaded_tk = NativeToken.from_state(mock_app, state)
        assert loaded_tk.sid == sid


def test_native_token_session_dict_write(mock_app, sid_tokens):
    random.shuffle(sid_tokens)
    for sid, tk in sid_tokens:
        state = tk.to_state()
        loaded_tk = NativeToken.from_state(mock_app, state)
        loaded_tk.session['Asid'] = 'A' + sid


def test_native_token_session_dict_read(mock_app, sid_tokens):
    random.shuffle(sid_tokens)
    for sid, tk in sid_tokens:
        state = tk.to_state()
        loaded_tk = NativeToken.from_state(mock_app, state)
        loaded_tk.session['Asid'] = 'A' + sid

    random.shuffle(sid_tokens)
    for sid, tk in sid_tokens:
        loaded_tk = NativeToken(mock_app, 'dummy', sid)
        assert loaded_tk.session['Asid'] == 'A' + sid
