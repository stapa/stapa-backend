import pytest
from uuid import uuid4
from datetime import datetime, timedelta

from sqlalchemy import select, create_engine
from sqlalchemy.orm import Session
from sqlalchemy.exc import MultipleResultsFound, IntegrityError


from stapa.backend.db.database import Base
from stapa.backend.db.models import User, Poll, PollMemberVoted, PollVote


@pytest.fixture
def engine(scope='session'):
    engine = create_engine("postgresql+psycopg2://stapa_tests:stapa_tests@localhost/stapa_test")
    Base.metadata.create_all(engine)
    yield engine
    Base.metadata.drop_all(engine)
    engine.dispose()


def test_create_schema(engine):
    pass


def create_poll(ref=None):
    return Poll(
        description=ref if ref else "Test Poll"
    )


def test_create_poll(engine):
    test_poll = create_poll()   
    with Session(engine) as session:
        session.add(test_poll)
        session.commit()

def test_retrieve_poll(engine):
    test_poll = create_poll()
    with Session(engine) as session:
        session.add(test_poll)
        session.commit()
        poll_id = test_poll.id

    with Session(engine) as session:
        query1 = select(Poll).where(Poll.id == poll_id)
        res1 = session.execute(query1).scalar_one_or_none()
        assert res1.description == "Test Poll"

def test_check_open_status_not_before(engine):
    with Session(engine) as session:
        poll1 = create_poll(ref='notbefore_passed')
        poll1.not_before = datetime.utcnow() - timedelta(days=1)
        poll2 = create_poll(ref='notbefore_notpassed')
        poll2.not_before = datetime.utcnow() + timedelta(days=1)
        poll3 = create_poll(ref='notbefore_unset')
        session.add_all([poll1, poll2, poll3])
        session.commit()
    with Session(engine) as session:
        # hybrid property: test DB side (query)
        res_true = session.scalars(select(Poll).where(Poll.open)).all()
        res_false = session.scalars(select(Poll).where(~Poll.open)).all()
        assert len(res_true) == 2
        assert len(res_false) == 1
        assert set([p.description for p in res_true]) == {'notbefore_passed', 'notbefore_unset'}
        assert set([p.description for p in res_false]) == {'notbefore_notpassed'}
    with Session(engine) as session:
        # hybrid property: test Python side (object)
        res = session.scalars(select(Poll)).all()
        res_true = [r for r in res if r.open == True]
        res_false = [r for r in res if r.open == False]
        assert len(res_true) == 2
        assert len(res_false) == 1
        assert set([p.description for p in res_true]) == {'notbefore_passed', 'notbefore_unset'}
        assert set([p.description for p in res_false]) == {'notbefore_notpassed'}


def test_check_executable_status_not_after(engine):
    with Session(engine) as session:
        poll1 = create_poll(ref='notafter_passed')
        poll1.not_after = datetime.utcnow() - timedelta(days=1)
        poll2 = create_poll(ref='notafter_notpassed')
        poll2.not_after = datetime.utcnow() + timedelta(days=1)
        poll3 = create_poll(ref='notafter_unset')
        session.add_all([poll1, poll2, poll3])
        session.commit()
    with Session(engine) as session:
        # hybrid property: test DB side (query)
        res_true = session.scalars(select(Poll).where(Poll.open)).all()
        res_false = session.scalars(select(Poll).where(~Poll.open)).all()
        assert len(res_true) == 2
        assert len(res_false) == 1
        assert set([p.description for p in res_true]) == {'notafter_notpassed', 'notafter_unset'}
        assert set([p.description for p in res_false]) == {'notafter_passed'}
    with Session(engine) as session:
        # hybrid property: test Python side (object)
        res = session.scalars(select(Poll)).all()
        res_true = [r for r in res if r.open == True]
        res_false = [r for r in res if r.open == False]
        assert len(res_true) == 2
        assert len(res_false) == 1
        assert set([p.description for p in res_true]) == {'notafter_notpassed', 'notafter_unset'}
        assert set([p.description for p in res_false]) == {'notafter_passed'}

def test_no_double_votes(engine):
    with Session(engine) as session:
        poll1 = Poll(description="Poll 1")
        poll2 = Poll(description="Poll 2")
        user1 = User(display_name="User 1")
        user2 = User(display_name="User 2")
        session.add_all([poll1, poll2, user1, user2])
        session.commit()
        user1_id = user1.id
        user2_id = user2.id
        poll1_id = poll1.id
        poll2_id = poll2.id

    def mark_voted(user_id, poll_id):
        with Session(engine) as session:
            user = session.scalars(select(User).where(User.id == user_id)).one()
            poll = session.scalars(select(Poll).where(Poll.id == poll_id)).one()
            session.add(PollMemberVoted(user=user, poll=poll))
            session.commit()

    mark_voted(user1_id, poll1_id)
    with pytest.raises(IntegrityError):
        mark_voted(user1_id, poll1_id)
    mark_voted(user2_id, poll1_id)
    mark_voted(user1_id, poll2_id)
    mark_voted(user2_id, poll2_id)
    with pytest.raises(IntegrityError):
        mark_voted(user2_id, poll1_id)
    with pytest.raises(IntegrityError):
        mark_voted(user1_id, poll2_id)
    with pytest.raises(IntegrityError):
        mark_voted(user2_id, poll2_id)

