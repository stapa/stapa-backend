import os
from pathlib import Path
from secrets import token_urlsafe
from uuid import uuid4

import pytest

from stapa.backend.config import settings


@pytest.fixture
def secrets_file(tmp_path):
    secrets_file = Path(tmp_path) / '.secrets.toml'
    return secrets_file


def test_default_config():
    from stapa.backend.config import settings
    assert settings.ENV_NAME == 'development'


def test_secret_loading(secrets_file):
    azure_client_id = str(uuid4())
    azure_client_secret = token_urlsafe(32)
    db_password = token_urlsafe(16)

    with secrets_file.open('w') as fp:
        fp.write(f"""\
            [development]
            AZURE_CLIENT_ID = "{azure_client_id}"
            AZURE_CLIENT_SECRET = "{azure_client_secret}" # expires 2025-03-01
            DB_PASSWORD = "{db_password}"
        """)

    os.environ['STAPA_SECRETS'] = str(secrets_file)
    settings.reload_secrets()
    assert settings.AZURE_CLIENT_ID == azure_client_id
    assert settings.AZURE_CLIENT_SECRET == azure_client_secret
    assert settings.DB_PASSWORD == db_password


def test_env_testing():
    os.environ['STAPA_MODE'] = 'testing'
    settings.reload()
    assert settings.ENV_NAME == 'testing'
