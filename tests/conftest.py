import json
import re
from urllib.parse import urljoin

import pytest
import requests
from lxml.etree import HTML


def mslogin_conf(res):
    mslogin_conf_m = re.compile(r'//<!\[CDATA\[\n\$Config=(.+);\n//\]\]>')
    scripts = HTML(res.content.decode('utf-8')).xpath('/html/head/script')
    for script in scripts:
        if (s := script.text) and (m := mslogin_conf_m.match(s)):
            return json.loads(m.groups()[0])


class Helpers:
    @staticmethod
    def perform_msal_login(login_url, username, password):
        s = requests.Session()
        s.headers['User-Agent'] = 'AzureAutomatedIntegrationTest'
        res = s.get(login_url)
        conf = mslogin_conf(res)
        assert conf['urlPost'].endswith('/login')
        res = s.post(
            urljoin(login_url, conf['urlPost']),
            data={'login': username,
                  'passwd': password,
                  'type': 11,
                  'canary': conf['canary'],
                  'ctx': conf['sCtx'],
                  'flowToken': conf['sFT'],
                  'hpgrequestid': conf['sessionId']}
        )
        conf = mslogin_conf(res)
        assert not conf['urlPost'].endswith('/login'), "Login failed"
        assert conf['urlPost'] == '/kmsi'
        res = s.post(
            urljoin(login_url, conf['urlPost']),
            data={'LoginOptions': 3,
                  'type': 28,
                  'canary': conf['canary'],
                  'ctx': conf['sCtx'],
                  'flowToken': conf['sFT'],
                  'hpgrequestid': conf['sessionId']},
            allow_redirects=False
        )
        assert res.status_code == 302
        return res.headers['Location']


@pytest.fixture
def helpers():
    return Helpers
