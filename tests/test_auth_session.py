from secrets import token_urlsafe
from urllib.parse import urlparse, parse_qs

import pytest
import requests
from sqlalchemy import create_engine

from stapa.backend.config import settings
settings.DB_URL = "postgresql+psycopg2://stapa_tests:stapa_tests@localhost/stapa_test"

import stapa.backend.app
from stapa.backend.db.database import Base


@pytest.fixture
def app():
    engine = create_engine(settings.DB_URL)
    Base.metadata.create_all(engine)
    engine.dispose()
    app = stapa.backend.app.app
    yield app
    engine = create_engine(settings.DB_URL)
    Base.metadata.drop_all(engine)
    engine.dispose()



@pytest.fixture
def client(app):
    return app.test_client()


def test_login_get(client):
    res = client.get('/auth/login')
    assert res.status_code == 405


def test_login_post_urlform(client):
    res = client.post('/auth/login', data={})
    assert res.status_code == 400


def test_login_post_json_invalid(client):
    res = client.post('/auth/login', json='')
    assert res.status_code == 400


def test_login_post_json_missing_redirect(client):
    res = client.post('/auth/login', json={})
    assert res.status_code == 400


def test_login_post_json_unauthorized_redirect(client):
    settings.AUTHORIZED_REDIRECTS = ['https://test-authorized-redirect.com']
    res = client.post('/auth/login',
                      json={'redirect_url': 'http://test-unauthorized-redirect.org'})
    assert res.status_code == 400


def test_login_post_json_authorized_redirect(client):
    settings.AUTHORIZED_REDIRECTS = ['https://test-authorized-redirect.com']
    res = client.post('/auth/login',
                      json={'redirect_url': 'https://test-authorized-redirect.com'})
    assert res.status_code == 200


def test_login_post_json_authorized_wildcard(client):
    settings.AUTHORIZED_REDIRECTS = ['https://*.wildcard.com']
    res = client.post('/auth/login', json={'redirect_url': 'https://test123.wildcard.com'})
    assert res.status_code == 200
    res = client.post('/auth/login', json={'redirect_url': 'https://test-with-dashes-123.wildcard.com'})
    assert res.status_code == 200
    res = client.post('/auth/login', json={'redirect_url': 'https://test-with-slash/bypass.wildcard.com'})
    assert res.status_code == 400
    res = client.post('/auth/login', json={'redirect_url': 'https://test-with-query?bypass.wildcard.com'})
    assert res.status_code == 400
    res = client.post('/auth/login', json={'redirect_url': 'starthttps://test-start.wildcard.com'})
    assert res.status_code == 400
    res = client.post('/auth/login', json={'redirect_url': 'https://test-end.wildcard.comend'})
    assert res.status_code == 400
    res = client.post('/auth/login', json={'redirect_url': 'https://testdot.wildcarddcom'})
    assert res.status_code == 400


def test_login_post_json(client):
    res = client.post('/auth/login',
                      json={'redirect_url': settings.AUTHORIZED_REDIRECTS[0]})
    assert res.status_code == 200
    login_url = res.json['login_url']
    assert login_url.startswith(settings.AZURE_AUTHORITY
                                + '/oauth2/v2.0/authorize')
    query = parse_qs(urlparse(login_url).query)
    assert query['client_id'][0] == settings.AZURE_CLIENT_ID
    assert query['response_type'][0] == 'code'
    assert not query['state'][0].startswith('nativeToken:')


def test_login_post_json_native(client):
    res = client.post('/auth/login',
                      json={'redirect_url': settings.AUTHORIZED_REDIRECTS[0],
                            'native_app': True})
    assert res.status_code == 200
    login_url = res.json['login_url']
    query = parse_qs(urlparse(login_url).query)
    assert query['state'][0].startswith('nativeToken:')

# integration tests


def test_integration_wrong_login(client, helpers):
    res = client.post(
        '/auth/login',
        json={'redirect_url': settings.AUTHORIZED_REDIRECTS[0]}
    )
    with pytest.raises(AssertionError, match='Login failed'):
        helpers.perform_msal_login(
            res.json['login_url'],
            'fake@microsoft.com',
            token_urlsafe()  # a random invalid password
        )


def test_integration_callback_url(client, helpers):
    res = client.post(
        '/auth/login',
        json={'redirect_url': settings.AUTHORIZED_REDIRECTS[0]}
    )
    callback_url = helpers.perform_msal_login(
        res.json['login_url'],
        settings.TESTING_INTEGRATION_AZURE_USERNAME,
        settings.TESTING_INTEGRATION_AZURE_PASSWORD
    )
    callback_url_parts = urlparse(callback_url)
    assert callback_url_parts.scheme == 'http'
    # apparently when running in TestClient url_for(..., _external=True)
    # returns http://localhost/<route> without port.
    assert callback_url_parts.netloc == 'localhost'
    assert callback_url_parts.path == '/auth/msalCallback'


def test_integration_callback_nocontext(app, helpers):
    client1 = app.test_client()
    client2 = app.test_client()
    res = client1.post(
        '/auth/login',
        json={'redirect_url': settings.AUTHORIZED_REDIRECTS[0]}
    )
    callback_url = helpers.perform_msal_login(
        res.json['login_url'],
        settings.TESTING_INTEGRATION_AZURE_USERNAME,
        settings.TESTING_INTEGRATION_AZURE_PASSWORD
    )
    callback_url_parts = urlparse(callback_url)

    res = client2.get('/auth/msalCallback?' + callback_url_parts.query)
    assert res.status_code == 400
    assert res.json['description'] == 'Invalid session cookie.'


def test_integration_callback_native(app, helpers):
    client_native = app.test_client()
    client_browser = app.test_client()
    res = client_native.post(
        '/auth/login',
        json={'redirect_url': settings.AUTHORIZED_REDIRECTS[0],
              'native_app': True}
    )
    callback_url = helpers.perform_msal_login(
        res.json['login_url'],
        settings.TESTING_INTEGRATION_AZURE_USERNAME,
        settings.TESTING_INTEGRATION_AZURE_PASSWORD
    )
    callback_url_parts = urlparse(callback_url)

    res = client_browser.get('/auth/msalCallback?' + callback_url_parts.query)
    assert res.status_code == 302
    assert res.headers['location'] == settings.AUTHORIZED_REDIRECTS[0]

    res = client_native.post(
        '/auth/login',
        json={'redirect_url': settings.AUTHORIZED_REDIRECTS[0],
              'native_app': True}
    )
    client_native_cookies = requests.utils.dict_from_cookiejar(
        client_native.cookie_jar)
    client_browser_cookies = requests.utils.dict_from_cookiejar(
        client_browser.cookie_jar)
    assert (client_native_cookies[app.config['SESSION_COOKIE_NAME']]
            != client_browser_cookies[app.config['SESSION_COOKIE_NAME']])
    assert res.status_code == 400
    assert res.json['description'] == "Already logged in."
