import pytest
from uuid import uuid4

from sqlalchemy import select, create_engine
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError


from stapa.backend.db.database import Base
from stapa.backend.db.models import User, Subscription


@pytest.fixture
def engine():
    #engine = create_engine("sqlite:////tmp/test.db")
    engine = create_engine("postgresql+psycopg2://stapa_tests:stapa_tests@localhost/stapa_test")
    Base.metadata.create_all(engine)
    yield engine
    Base.metadata.drop_all(engine)
    engine.dispose()    

def test_create_schema(engine):
    pass


def create_user():
    return User(
        given_name="Test",
        family_name="User",
        display_name="Test User",
        pasteur_azure_ad_oid=uuid4()
    )


def test_create_user(engine):
    test_user = create_user()
    with Session(engine) as session:
        session.add(test_user)
        session.commit()


def test_retrieve_user(engine):
    test_user = create_user()
    oid = test_user.pasteur_azure_ad_oid
    with Session(engine) as session:
        session.add(test_user)
        session.commit()

    with Session(engine) as session:
        query = select(User).where(User.pasteur_azure_ad_oid == oid)
        res = session.execute(query).scalar_one_or_none()
        assert res.given_name == "Test"
        assert res.family_name == "User"


def test_add_duplicated_oid(engine):
    test_user1 = create_user()
    test_user2 = create_user()

    oid = test_user1.pasteur_azure_ad_oid
    test_user2.pasteur_azure_ad_oid = oid
    with Session(engine) as session:
        session.add(test_user1)
        session.add(test_user2)
        with pytest.raises(IntegrityError):
            session.commit()


def test_retrieve_unknown_oid(engine):
    with Session(engine) as session:
        query = select(User).where(User.pasteur_azure_ad_oid == uuid4())
        res = session.execute(query).scalar_one_or_none()
        assert res is None
