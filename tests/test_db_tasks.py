import pytest
from uuid import uuid4
from datetime import datetime, timedelta

from sqlalchemy import select, create_engine
from sqlalchemy.orm import Session
from sqlalchemy.exc import MultipleResultsFound


from stapa.backend.db.database import Base
from stapa.backend.db.models import Task


@pytest.fixture
def engine(scope='session'):
    #engine = create_engine("sqlite:////tmp/test.db")
    engine = create_engine("postgresql+psycopg2://stapa_tests:stapa_tests@localhost/stapa_test")
    Base.metadata.create_all(engine)
    yield engine
    Base.metadata.drop_all(engine)
    engine.dispose()


def test_create_schema(engine):
    pass


def create_task(ref=None):
    return Task(
        task={'action': 'subscribe_stapa-all', 'azure_oid': '666', 'ref': ref}
    )


def test_create_task(engine):
    test_task = create_task()   
    with Session(engine) as session:
        session.add(test_task)
        session.commit()


def test_retrieve_task(engine):
    test_task = create_task()
    action = test_task.task['action']
    oid = test_task.task['azure_oid']
    with Session(engine) as session:
        session.add(test_task)
        session.commit()

    with Session(engine) as session:
        query1 = select(Task).where(Task.task['action'].astext == action)
        res1 = session.execute(query1).scalar_one_or_none()
        assert res1.task['action'] == action
        assert res1.task['azure_oid'] == oid
        assert res1.status == 'new'

    with Session(engine) as session:
        query2 = select(Task).where(Task.task['azure_oid'].astext == oid)
        res2 = session.execute(query2).scalar_one_or_none()
        assert res2.task['action'] == action
        assert res2.task['azure_oid'] == oid
        assert res2.status == 'new'


def test_check_executable_status_new(engine):
    with Session(engine) as session:
        task = create_task()
        session.add(task)
        session.commit()
    with Session(engine) as session:
        # hybrid property: test DB side (query)
        res_true = session.scalars(select(Task).where(Task.executable)).all()
        res_false = session.scalars(select(Task).where(~Task.executable)).all()
        assert len(res_true) == 1
        assert len(res_false) == 0
    with Session(engine) as session:
        # hybrid property: test Python side (object)
        res = session.scalars(select(Task)).all()
        assert res[0].executable == True


def test_check_executable_status_executed(engine):
    with Session(engine) as session:
        task = create_task()
        task.status = 'executed'
        session.add(task)
        session.commit()
    with Session(engine) as session:
        # hybrid property: test DB side (query)
        res_true = session.scalars(select(Task).where(Task.executable)).all()
        res_false = session.scalars(select(Task).where(~Task.executable)).all()
        assert len(res_true) == 0
        assert len(res_false) == 1
    with Session(engine) as session:
        # hybrid property: test Python side (object)
        res = session.scalars(select(Task)).all()
        assert res[0].executable == False


def test_check_executable_status_not_before(engine):
    with Session(engine) as session:
        task1 = create_task(ref='notbefore_passed')
        task1.not_before = datetime.utcnow() - timedelta(days=1)
        task2 = create_task(ref='notbefore_notpassed')
        task2.not_before = datetime.utcnow() + timedelta(days=1)
        task3 = create_task(ref='notbefore_unset')
        session.add(task1)
        session.add(task2)
        session.add(task3)
        session.commit()
    with Session(engine) as session:
        # hybrid property: test DB side (query)
        res_true = session.scalars(select(Task).where(Task.executable)).all()
        res_false = session.scalars(select(Task).where(~Task.executable)).all()
        assert len(res_true) == 2
        assert len(res_false) == 1
        assert set([t.task.get('ref') for t in res_true]) == {'notbefore_passed', 'notbefore_unset'}
        assert set([t.task.get('ref') for t in res_false]) == {'notbefore_notpassed'}
    with Session(engine) as session:
        # hybrid property: test Python side (object)
        res = session.scalars(select(Task)).all()
        res_true = [r for r in res if r.executable == True]
        res_false = [r for r in res if r.executable == False]
        assert len(res_true) == 2
        assert len(res_false) == 1
        assert set([t.task.get('ref') for t in res_true]) == {'notbefore_passed', 'notbefore_unset'}
        assert set([t.task.get('ref') for t in res_false]) == {'notbefore_notpassed'}


def test_check_executable_status_not_after(engine):
    with Session(engine) as session:
        task1 = create_task(ref='notafter_passed')
        task1.not_after = datetime.utcnow() - timedelta(days=1)
        task2 = create_task(ref='notafter_notpassed')
        task2.not_after = datetime.utcnow() + timedelta(days=1)
        task3 = create_task(ref='notafter_unset')
        session.add(task1)
        session.add(task2)
        session.add(task3)
        session.commit()
    with Session(engine) as session:
        # hybrid property: test DB side (query)
        res_true = session.scalars(select(Task).where(Task.executable)).all()
        res_false = session.scalars(select(Task).where(~Task.executable)).all()
        assert len(res_true) == 2
        assert len(res_false) == 1
        assert set([t.task.get('ref') for t in res_true]) == {'notafter_notpassed', 'notafter_unset'}
        assert set([t.task.get('ref') for t in res_false]) == {'notafter_passed'}
    with Session(engine) as session:
        # hybrid property: test Python side (object)
        res = session.scalars(select(Task)).all()
        res_true = [r for r in res if r.executable == True]
        res_false = [r for r in res if r.executable == False]
        assert len(res_true) == 2
        assert len(res_false) == 1
        assert set([t.task.get('ref') for t in res_true]) == {'notafter_notpassed', 'notafter_unset'}
        assert set([t.task.get('ref') for t in res_false]) == {'notafter_passed'}

def test_check_executable_status_not_after_and_executed(engine):
    with Session(engine) as session:
        task1 = create_task(ref='notafter_passed_executed')
        task1.status = 'executed'
        task1.not_after = datetime.utcnow() - timedelta(days=1)
        task2 = create_task(ref='notafter_notpassed_executed')
        task2.status = 'executed'
        task2.not_after = datetime.utcnow() + timedelta(days=1)
        task3 = create_task(ref='notafter_passed')
        task3.not_after = datetime.utcnow() - timedelta(days=1)
        task4 = create_task(ref='notafter_notpassed')
        task4.not_after = datetime.utcnow() + timedelta(days=1)
        session.add(task1)
        session.add(task2)
        session.add(task3)
        session.add(task4)
        session.commit()
    with Session(engine) as session:
        # hybrid property: test DB side (query)
        res_true = session.scalars(select(Task).where(Task.executable)).all()
        res_false = session.scalars(select(Task).where(~Task.executable)).all()
        assert len(res_true) == 1
        assert len(res_false) == 3
        assert set([t.task.get('ref') for t in res_true]) == {'notafter_notpassed'}
        assert set([t.task.get('ref') for t in res_false]) == {'notafter_passed', 'notafter_passed_executed', 'notafter_notpassed_executed'}
    with Session(engine) as session:
        # hybrid property: test Python side (object)
        res = session.scalars(select(Task)).all()
        res_true = [r for r in res if r.executable == True]
        res_false = [r for r in res if r.executable == False]
        assert len(res_true) == 1
        assert len(res_false) == 3
        assert set([t.task.get('ref') for t in res_true]) == {'notafter_notpassed'}
        assert set([t.task.get('ref') for t in res_false]) == {'notafter_passed', 'notafter_passed_executed', 'notafter_notpassed_executed'}