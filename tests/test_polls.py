import pytest
from sqlalchemy import create_engine, select
import datetime

from stapa.backend.config import settings
settings.DB_URL = "postgresql+psycopg2://stapa_tests:stapa_tests@localhost/stapa_test"

import stapa.backend.app
from stapa.backend.db.database import Base
from stapa.backend.db.models import User, Poll
from itsdangerous.url_safe import URLSafeSerializer


@pytest.fixture
def app():
    engine = create_engine(settings.DB_URL)
    Base.metadata.create_all(engine)
    engine.dispose()
    app = stapa.backend.app.app
    yield app
    engine = create_engine(settings.DB_URL)
    Base.metadata.drop_all(engine)
    engine.dispose()


@pytest.fixture
def app_and_tokens(app):
    with app.app_context():
        user1 = User(display_name="user1")
        user2 = User(display_name="user2")
        poll1 = Poll(description="poll1")
        poll2 = Poll(description="poll2")
        app.db.add_all([user1, user2, poll1, poll2])
        app.db.commit()
        ser = URLSafeSerializer(settings.APP_SECRET_KEY)
        tokens = {
            (1, 1): ser.dumps({'user_id': user1.id, 'poll_id': poll1.id}),
            (1, 2): ser.dumps({'user_id': user1.id, 'poll_id': poll2.id}),
            (2, 1): ser.dumps({'user_id': user2.id, 'poll_id': poll1.id}),
            (2, 2): ser.dumps({'user_id': user2.id, 'poll_id': poll2.id})
        }
    return app, tokens

@pytest.fixture
def client_and_tokens(app_and_tokens):
    app, tokens = app_and_tokens
    return app.test_client(), tokens


def test_no_auth_token(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.get('/polls/1/token')
    assert res.status_code == 400
    assert res.json['description'] == "Invalid request, missing auth token"


def test_invalid_token(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.get('/polls/1/token?auth_token=eyfdsafdsaf')
    assert res.status_code == 400
    assert res.json['description'] == "Invalid request, bad auth token"


def test_valid_token(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.get(f'/polls/1/token?auth_token={tokens[(1, 1)]}')
    assert res.status_code == 200
    assert res.json['open'] == True
    assert res.json['voted'] == False


def test_valid_token_wrong_poll(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.get(f'/polls/2/token?auth_token={tokens[(1, 1)]}')
    assert res.status_code == 400


def test_vote_valid_token_malformed_vote(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.get(f'/polls/1/token?auth_token={tokens[(1, 1)]}')
    res = client.post(f'/polls/1/token?auth_token={res.json["auth_token"]}', json={
        'director': 1,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4 # ipin is missing
    })

    assert res.status_code == 400


def test_vote_valid_token_valid_vote(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.get(f'/polls/1/token?auth_token={tokens[(1, 1)]}')
    res = client.post(f'/polls/1/token?auth_token={res.json["auth_token"]}', json={
        'director': 1,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4,
        'ipin': None
    })

    assert res.status_code == 204

def test_vote_valid_token_double_vote_rejected(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.get(f'/polls/1/token?auth_token={tokens[(1, 1)]}')
    auth_token = res.json["auth_token"]
    res = client.post(f'/polls/1/token?auth_token={auth_token}', json={
        'director': 1,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4,
        'ipin': None
    })
    assert res.status_code == 204
    res = client.post(f'/polls/1/token?auth_token={auth_token}', json={
        'director': 1,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4,
        'ipin': None
    })
    assert res.status_code == 403

def test_vote_valid_voted_updated(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.get(f'/polls/1/token?auth_token={tokens[(1, 1)]}')
    assert res.json['voted'] == False
    auth_token = res.json["auth_token"]
    res = client.post(f'/polls/1/token?auth_token={auth_token}', json={
        'director': 1,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4,
        'ipin': None
    })
    assert res.status_code == 204
    res = client.get(f'/polls/1/token?auth_token={tokens[(1, 1)]}')
    assert res.json['voted'] == True

def test_stats(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.get(f'/polls/1')
    assert res.status_code == 200
    assert res.json['status'] == 'open'
    assert res.json['valid_votes'] == 0


def test_stats_after_vote(client_and_tokens):
    client, tokens = client_and_tokens
    res = client.post(f'/polls/1/token?auth_token={tokens[(1, 1)]}', json={
        'director': 1,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4,
        'ipin': None
    })
    res = client.get(f'/polls/1')
    assert res.status_code == 200
    assert res.json['status'] == 'open'
    assert res.json['valid_votes'] == 1
    res = client.post(f'/polls/1/token?auth_token={tokens[(2, 1)]}', json={
        'director': 42,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4,
        'ipin': 1
    })
    res = client.get(f'/polls/1')
    assert res.status_code == 200
    assert res.json['status'] == 'open'
    assert res.json['valid_votes'] == 2
    res = client.post(f'/polls/1/token?auth_token={tokens[(1, 2)]}', json={
        'director': 42,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4,
        'ipin': 1
    })
    res = client.get(f'/polls/1')
    assert res.status_code == 200
    assert res.json['status'] == 'open'
    assert res.json['valid_votes'] == 2


def test_results_publishing(app, client_and_tokens):
    with app.app_context():
        poll = app.db.scalars(select(Poll).where(Poll.id == 1)).one()
        poll.not_after = datetime.datetime.utcnow() + datetime.timedelta(hours=1)
        app.db.commit()

    client, tokens = client_and_tokens
    res = client.post(f'/polls/1/token?auth_token={tokens[(1, 1)]}', json={
        'director': 1,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4,
        'ipin': None
    })
    res = client.post(f'/polls/1/token?auth_token={tokens[(2, 1)]}', json={
        'director': 42,
        'vicedirector': None,
        'treasurer': None,
        'secretary': 4,
        'ipin': 1
    })
    res = client.get('/polls/1')
    assert res.json['status'] == 'open'
    assert res.json['remaining_seconds'] > 0
    assert res.json['valid_votes'] == 2
    with app.app_context():
        poll = app.db.scalars(select(Poll).where(Poll.id == 1)).one()
        poll.not_after = datetime.datetime.utcnow() - datetime.timedelta(minutes=5)
        app.db.commit()
    res = client.get('/polls/1')
    assert res.json['status'] == 'completed'
    assert res.json['remaining_seconds'] == 0
    assert res.json['valid_votes'] == 2
    assert set(res.json['results']['director'].keys()) == {'1', '42'}
    assert res.json['results']['director']['1'] == 1
    assert res.json['results']['director']['42'] == 1
    assert set(res.json['results']['vicedirector'].keys()) == {'0'}
    assert res.json['results']['vicedirector']['0'] == 2
    assert set(res.json['results']['treasurer'].keys()) == {'0'}
    assert res.json['results']['treasurer']['0'] == 2
    assert set(res.json['results']['secretary'].keys()) == {'4'}
    assert res.json['results']['secretary']['4'] == 2
    assert set(res.json['results']['ipin'].keys()) == {'0', '1'}
    assert res.json['results']['ipin']['0'] == 1
    assert res.json['results']['ipin']['1'] == 1






