"""add_tx_operator_and_metadata

Revision ID: 83a11be1ad2a
Revises: cd4ba74b6869
Create Date: 2023-10-02 20:20:53.745889

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '83a11be1ad2a'
down_revision = 'cd4ba74b6869'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('stripe_payment_intents', 'amount',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.add_column('wallet_transactions', sa.Column('reason', sa.Enum('card_topup', 'free_drink', 'paid_drinks', native_enum=False), nullable=False))
    op.add_column('wallet_transactions', sa.Column('operator_id', sa.Integer(), nullable=True))
    op.add_column('wallet_transactions', sa.Column('stripe_payment_intent_id', sa.String(), nullable=True))
    op.create_foreign_key(None, 'wallet_transactions', 'stripe_payment_intents', ['stripe_payment_intent_id'], ['id'])
    op.create_foreign_key(None, 'wallet_transactions', 'users', ['operator_id'], ['id'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'wallet_transactions', type_='foreignkey')
    op.drop_constraint(None, 'wallet_transactions', type_='foreignkey')
    op.drop_column('wallet_transactions', 'stripe_payment_intent_id')
    op.drop_column('wallet_transactions', 'operator_id')
    op.drop_column('wallet_transactions', 'reason')
    op.alter_column('stripe_payment_intents', 'amount',
               existing_type=sa.INTEGER(),
               nullable=True)
    # ### end Alembic commands ###
