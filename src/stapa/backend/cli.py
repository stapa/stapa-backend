import argparse
import datetime

from dateutil.relativedelta import relativedelta
import waitress

from . import app
from .db.database import Base, engine, SessionLocal
from .db.models import SubscriptionOption

import logging

logger = logging.getLogger(__name__)


def install():
    Base.metadata.create_all(engine)


def create_membership_option():
    from .stripe import StripeClient

    parser = argparse.ArgumentParser()
    today = datetime.datetime.today().isoformat().split('T')[0]
    parser.add_argument('--name', action="store", help='Option name',
                        default='StaPa Membership')
    parser.add_argument('price', action="store", help='Option price in cents')
    parser.add_argument('--start', action="store",
                        help=(
                            'Starting date YYYY-MM-DD, '
                            'use subscription moving windows if omitted'
                        ))
    parser.add_argument('-d', '--duration', action="store",
                        help='Duration in months', default=6)
    parser.add_argument('--desc',
                        help='Descriptor to report in member bank statements')
    args = parser.parse_args()

    stripe = StripeClient()
    absolute = args.start is not None
    if absolute:
        valid_from = datetime.datetime.fromisoformat(args.start)
        valid_to = (valid_from
                    + relativedelta(months=int(args.duration)))
        stripe_price_id = stripe.create_price(
            args.name + f" from {valid_from:%d/%m/%Y} to {valid_to:%d/%m/%Y}",
            args.price,
            statement_descriptor=args.desc
        )
        sub = SubscriptionOption(
            name=args.name,
            price=args.price,
            stripe_price_id=stripe_price_id,
            type='absolute',
            valid_from=valid_from,
            valid_to=valid_to,
            selectable_from=valid_from,
            selectable_to=valid_to - datetime.timedelta(days=30)
        )
    else:
        valid_for_months = int(args.duration)
        stripe_price_id = stripe.create_price(
            args.name + f" for {args.duration} months",
            args.price,
            statement_descriptor=args.desc
        )
        sub = SubscriptionOption(
            name=args.name,
            price=args.price,
            stripe_price_id=stripe_price_id,
            type='moving',
            valid_for_months=valid_for_months,
            selectable_from=datetime.datetime.utcnow(),
            selectable_to=(datetime.datetime.utcnow()
                           + relativedelta(months=valid_for_months))
        )

    db = SessionLocal()
    db.add(sub)
    db.commit()
    logger.info(
        f"Option created: {sub.name}. "
        + f"Selectable from: {sub.selectable_from}"
        + f" to: {sub.selectable_to}, "
        + (f"valid from: {sub.valid_from} to: {sub.valid_to}, "
           if absolute
           else f"valid for: {sub.valid_for_months} months, ")
        + f"price: {sub.price/100:.2f} EUR"
    )


def serve():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--host', action="store", help='Listen address',
                        default='127.0.0.1')
    parser.add_argument('-p', '--port', action="store", help='Listen port',
                        default=5000)
    args = parser.parse_args()

    waitress.serve(app.app,
                   host=args.host,
                   port=args.port,
                   url_scheme='http')
