import pkgutil

from smtplib import SMTP
from string import Template
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

from .config import settings
from .db.models import User


def get_resource(name):
    return pkgutil.get_data(__package__, 'resources/' + name)


def send_welcome(user: User):
    text_template = get_resource('welcome.txt').decode('utf-8')
    html_template = get_resource('welcome.html').decode('utf-8')

    text = Template(text_template).substitute(member=user.given_name)
    html = Template(html_template).substitute(member=user.given_name)

    text_part = MIMEText(text, 'plain')
    html_part = MIMEText(html, 'html')

    msg_alternative = MIMEMultipart('alternative')
    msg_alternative.attach(text_part)
    msg_alternative.attach(html_part)

    msg_mixed = MIMEMultipart('mixed')
    msg_mixed.attach(msg_alternative)
    msg_mixed['From'] = 'StaPa Robot <robot@stapa.fr>'
    msg_mixed['To'] = user.pasteur_email_address
    msg_mixed['Subject'] = (
        f'{"[TEST] " if settings.env_name != "production" else ""}'
        f'🎉 StaPa Membership Validated!'
    )
    msg_mixed['Reply-To'] = 'stapa@pasteur.fr'

    s = SMTP(host=settings.SMTP_HOSTNAME, port=settings.SMTP_PORT)
    if settings.get('SMTP_USE_STARTTLS'):
        s.starttls()
    if settings.get('SMTP_USERNAME'):
        s.login(settings.SMTP_USERNAME, settings.SMTP_PASSWORD)
    s.sendmail(msg_mixed['From'], msg_mixed['To'], msg_mixed.as_string())
