import logging
import requests
import json

from .config import settings
from .db.models import User

logger = logging.getLogger(__name__)

try:
    import uwsgi
    is_uwsgi = True
except ImportError:
    is_uwsgi = False


class DolibarrException(Exception):
    pass


def update_member(user: User):
    if is_uwsgi:
        uwsgi.lock()
    try:
        res = requests.get(
            settings.DOLIBARR_ENDPOINT + f'/members/ref_ext/{user.id}',
            headers={'DOLAPIKEY': settings.DOLIBARR_SECRET_KEY})
        if res.status_code == 401:
            raise DolibarrException('Invalid api key!')
        elif res.status_code not in [200, 404]:
            raise DolibarrException('Invalid endpoint!')

        user_data = {
            'typeid': 1,
            'ref_ext': user.id,
            'morphy': 'phy',
            'firstname': user.given_name,
            'lastname': user.family_name,
            'email': user.email_address,
            'phone': user.phone_number,
            'birth': int(user.birthdate.strftime('%s')) + 43200 if user.birthdate else None,
            'note_public': f'({user.pronouns})' if user.pronouns else '',
            'note_private': json.dumps({
                'pasteur_matricule': user.pasteur_matricule,
                'pasteur_azure_ad_oid': str(user.pasteur_azure_ad_oid),
                'pasteur_email_address': user.pasteur_email_address,
                'pasteur_azure_ad_upn': user.pasteur_azure_ad_upn,
                'pasteur_position': user.pasteur_position.description if user.pasteur_position else user.pasteur_position_other,
                'stripe_customer_id': user.stripe_customer_id
            })
        }

        if res.status_code == 404:
            res = requests.post(
                settings.DOLIBARR_ENDPOINT + '/members',
                headers={'DOLAPIKEY': settings.DOLIBARR_SECRET_KEY},
                json=user_data)
            if res.status_code == 200:
                # Activate new user
                res = requests.put(
                    settings.DOLIBARR_ENDPOINT + f'/members/{int(res.json())}',
                    headers={'DOLAPIKEY': settings.DOLIBARR_SECRET_KEY},
                    json={'statut': '1'})
        else:
            res = requests.put(
                settings.DOLIBARR_ENDPOINT + f'/members/{res.json()["id"]}',
                headers={'DOLAPIKEY': settings.DOLIBARR_SECRET_KEY},
                json=user_data)

        if res.status_code == 200:
            user_id = res.json()["id"]
            logger.info(f'User {user.pasteur_azure_ad_upn} created/updated on '
                        f'Dolibarr with id: {user_id}')
        else:
            logger.error(f'Unable while creating {user.pasteur_azure_ad_upn} on '
                         f'Dolibarr, reason: {json.loads(res.content)}')
            user_id = None
    finally:
        if is_uwsgi:
            uwsgi.unlock()
    return user_id
