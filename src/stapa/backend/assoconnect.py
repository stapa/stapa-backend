import re
import requests
import logging
import datetime
from urllib.parse import urlencode

from lxml.etree import HTML

logger = logging.getLogger(__name__)


def post_form(s, url, data, form_id="sparkTemplateTableExportColumnsForm"):
    res = s.get(url)
    form = HTML(
        res.content.decode('utf-8')
    ).xpath(f'//form[@id="{form_id}"]')[0]

    def get_value(name):
        return form.xpath(f'input[@name="{name}"]')[0].attrib['value']
    init_data = {
        '_form': get_value('_form'),
        '_formMulti': get_value('_formMulti'),
        '_origin': get_value('_origin'),
        '_csrf': get_value('_csrf')
    }
    init_data.update(data)
    res = s.post(
        url,
        init_data
    )
    return res


class AccountingSubmission:
    def __init__(self, date=datetime.datetime.today(), user_id=None):
        self.date = date
        self.user_id = user_id
        self.groups = []

    def add_group(self, entries, comment='', ledger='OD'):
        self.groups.append({'ledger': ledger,
                            'comment': comment,
                            'entries': entries})

    def to_log(self):
        for group_idx, group in enumerate(self.groups):
            for entry in group['entries']:
                logger.info(
                    f'ACCOUTING ENTRY {self.date.strftime("%Y-%m-%d")} '
                    f'journal={group["ledger"]}, libellé=\'{entry.name}\', '
                    f'compte={entry.account}, debit={entry.debit}, '
                    f'credit={entry.credit}'
                )

    def to_dict(self):
        out = {
            'action': 'save',
            'BuyPackerAccountingEntryUserToggle': '1' if self.user_id else '',
            'BuyPackerAccountingEntryUserId': self.user_id,
            'BuyPackerAccountingEntryAccountingClientId': '0',
            'BuyPackerAccountingEntryAccountingSupplierId': '0',
        }

        entry_idx = 0
        for group_idx, group in enumerate(self.groups):
            prefix_group = f'BuyPackerRecords[{group_idx+1}]'
            out.update({
                prefix_group + '[id]': '',
                prefix_group + '[date]': self.date.strftime('%Y%m%d000000'),
                prefix_group + '[ledger]': group['ledger'],
                prefix_group + '[comment]': group['comment'],
                prefix_group + '[checkNumber]': ''
            })
            for entry in group['entries']:
                entry_idx += 1
                prefix_entry = (prefix_group
                                + f'[accountingEntry][{entry_idx}]')
                out.update({
                    prefix_entry + '[id]': '',
                    prefix_entry + '[name]': entry.name,
                    prefix_entry + '[account]': entry.account,
                    prefix_entry + '[debitAmount]': f'{entry.debit:.2f}',
                    prefix_entry + '[creditAmount]': f'{entry.credit:.2f}',
                })
        out.update({'uploadFile': [''] * len(self.groups)})
        return out


class AccountingEntry:
    def __init__(self, name, account, debit=0, credit=0):
        self.name = name
        self.account = account
        self.debit = debit
        self.credit = credit


class AssoConnectClientException(Exception):
    def __init__(self, message):
        logger.error(message)
        super().__init__(message)


class AssoConnectClient:
    def __init__(self, subdomain, username, password):
        self.subdomain = subdomain
        self.username = username
        self.password = password

        self.base = f'https://{subdomain}.assoconnect.com'
        self.base_m = self.base.replace('.', r'\.')

        self.algolia_api_key = None
        self.algolia_application_id = None
        self.asso_id = None

        self.s = requests.Session()
        self.s.headers.update(
            {'User-Agent': ('Mozilla/5.0 (X11; Linux x86_64; rv:109.0) '
                            'Gecko/20100101 Firefox/110.0')}
        )

    def _login(self):
        res = post_form(
            self.s, self.base + '/contacts/login',
            data={
                'userMail': self.username,
                'userPassword': self.password,
                'ssoToken': ''
            })

        if not re.match(self.base_m + '/page/[0-9]+-.+',
                        res.url):
            raise AssoConnectClientException(f'LOGIN: failed, url: {res.url}')

        res = self.s.get(self.base + '/admin/list')
        asso_id_m = re.match(self.base_m + '/organization/([0-9]+)/home',
                             res.url)
        if not asso_id_m:
            raise AssoConnectClientException('LOGIN: unable to fetch asso ID')
        self.asso_id = asso_id_m.groups()[0]

        res = self.s.get(self.base + f'/organization/contacts/{self.asso_id}')
        algolia_m = re.search(
            r'algoliasearch\(\s*"([^"]+)"\s*,\s*"([^"]+)"\s*\)', res.text)
        if not algolia_m:
            raise AssoConnectClientException(
                'LOGIN: unable to fetch Algolia keys'
            )
        self.algolia_application_id = algolia_m.groups()[0]
        self.algolia_api_key = algolia_m.groups()[1]

    def _search_by_mail(self, email):
        if not (self.algolia_api_key and self.algolia_application_id):
            raise AssoConnectClient('SEARCH: unable to search before login')
        res = self.s.post(
            (f'https://{self.algolia_application_id.lower()}-dsn.algolia.net'
             f'/1/indexes/BuyPacker_User-PROD/query'),
            headers={'X-Algolia-API-Key': self.algolia_api_key,
                     'X-Algolia-Application-Id': self.algolia_application_id},
            json={'params': urlencode({
                'attributesToRetrieve': '["id"]',
                'numericFilters': '["status=1","isStructure!=1"]',
                'restrictSearchableAttributes': '["mail"]',
                'query': email})
            }
        )
        data = res.json()
        if not data:
            raise AssoConnectClientException('SEARCH: query failed')
        return [hit['id'] for hit in data['hits']]

    def _obtain_user_id(self, firstname, lastname, email):
        results = self._search_by_mail(email)
        results.sort(reverse=True)
        if len(results) > 1:
            logger.warning(
                f"USER: Multiple users found with email '{email}': {results}"
                f" Using the most recent one: {results[0]}"
            )
        if results:
            logger.info(
                f"USER: Found existing user for '{email}': {results[0]},"
                f" no need to create a new one."
            )
            return results[0]
        else:
            res = post_form(
                self.s,
                self.base + f'/organization/contacts/create/{self.asso_id}',
                data={
                    'BuyPackerUserPictureIdFile': '',
                    'BuyPackerUserFirstname': firstname,
                    'BuyPackerUserLastname': lastname,
                    'BuyPackerUserMail': email,
                    'BuyPackerUserPhoneLandlineHelper': '',
                    'BuyPackerUserPhoneLandline': '',
                    'BuyPackerUserPhoneMobileHelper': '',
                    'BuyPackerUserPhoneMobile': '',
                    'BuyPackerUserAddressSearch': '',
                    'BuyPackerUserStreet1': '',
                    'BuyPackerUserStreet2': '',
                    'BuyPackerUserPostal': '',
                    'BuyPackerUserCity': '',
                    'BuyPackerUserCountry': '',
                    'BuyPackerUserAdministrativeArea1': '',
                    'BuyPackerUserAdministrativeArea2': '',
                    'BuyPackerUserLatitude': '',
                    'BuyPackerUserLongitude': ''
                }
            )
            if not re.match(
                (self.base_m
                 + r'/organization/contacts/[0-9]+\?messageSuccess.+'),
                res.url
            ):
                raise AssoConnectClientException(
                    f"USER: Unable to create an user for: '{email}'.")
            results = search_by_mail(email)
            results.sort(reverse=True)
            if not results:
                raise AssoConnectClientException(
                    f"USER: New user created for {email}, but unable to"
                    f" recover userId."
                )
            logger.info(f"USER: New user created for '{email}': {results[0]}")
            return results[0]

    def _create_membership_accounting_records(self, user_id, price, fee, name,
                                              email, transaction_id,
                                              transaction_stripe):
        comment = (
            f"Adhésion membre '{email}', voir transaction no. "
            f"{transaction_id}. Paiement geré hors AssoConnect."
        )
        comment_pay = (
            f"Adhésion membre '{email}', voir transaction no. "
            f"{transaction_id}. Paiement geré sur plateforme Stripe. "
            f"Transaction stripe no. {transaction_stripe}, montant credité sur"
            f" compte electronique Stripe."
        )
        accounting = AccountingSubmission(user_id=user_id)
        accounting.add_group(
            [AccountingEntry('Adhésion {name}',
                             '467000', debit=price),  # compte clients
             AccountingEntry('Adhésion {name}',
                             '756000', credit=price)],  # compte adhésions
            comment=comment, ledger='OD')
        accounting.add_group(
            [AccountingEntry('Adhésion {name} - paiement CB',
                             '467000', credit=price),  # compte clients
             AccountingEntry('Adhésion {name} - frais CB',
                             '627001', debit=fee),
             AccountingEntry('Adhésion {name} - paiement CB',
                             '517001', debit=price - fee)],  # compte Stripe
            comment=comment_pay, ledger='OD')

        res = post_form(
            self.s,
            (f'{self.base}/accounting/bookentry/entry/other-operations'
             f'/advanced-entry/{self.asso_id}?origin=bookEntry'),
            accounting.to_dict(),
            form_id='saveForm'
        )
        if not re.match(
            fr'{self.base_m}/accounting/bookentry/entry/other-operations'
            fr'/advanced-entry/{self.asso_id}\?messageSuccess%5B%5D=Les\+%C3'
            fr'%A9critures\+ont\+bien\+%C3%A9t%C3%A9\+enregistr%C3%A9es',
            res.url
        ):
            raise AssoConnectClientException('ACCOUNTING: un')

    def _create_membership(self, firstname, lastname, email,
                           form, price, stripe_fee):
        self.login()
        user_id = self._obtain_user_id(firstname, lastname, email)
        res = post_form(
            self.s, self.base + f'/collect/registrants/add/{form}',
            data={
                'BuyPackerSales[0][firstname]': firstname,
                'BuyPackerSales[0][lastname]': lastname,
                'BuyPackerSales[0][radio]': membership_price,
                'BuyPackerTransactionUserIdHelper': '',
                'BuyPackerTransactionUserId': user_id,
                'BuyPackerTransactionMail': '',
            })

        membership_m = re.match(
            self.base_m + '/collect/registrants/add/membership/([0-9]+)',
            res.url
        )
        if not membership_m:
            raise Excpetion('MEMBERSHIP: unable to create transaction')
        transaction_id = membership_m.groups()[0]
        logger.info(
            f"MEMBERSHIP: transaction created for '{email}': {transaction_id}")

        sale_inputs = HTML(res.content).xpath(
            '//input[starts-with(@id, "sale")]')
        sale_m = re.match(r'sale(\d+)UserId', sale_inputs[0].attrib['name'])
        if not sale_m:
            raise AssoConnectClientException(
                f"MEMBERSHIP: unable to associate a sale to"
                f" transaction '{transaction_id}' for '{email}'.")
        sale_id = sale_m.groups()[0]
        logger.info(
            f"MEMBERSHIP: sale {sale_id} associated to transaction"
            f" {transaction_id} for '{email}'"
        )
        res = post_form(
            self.s, res.url,
            data={
                f'sale{sale_id}UserId': user_id,
                f'sale{sale_id}SearchHelper': '',
                f'sale{sale_id}Search': '',
                f'sale{sale_id}UserFirstname': firstname,
                f'sale{sale_id}UserLastname': lastname,
                f'sale{sale_id}UserMail': email,
                f'sale{sale_id}UserPhoneMobileHelper': '',
                f'sale{sale_id}UserPhoneMobile': '',
            })

        payment_m = re.match(
            self.base_m + '/accounting/payment-registration/([0-9]+)',
            res.url
        )
        free_m = re.match(
            self.base_m + f'/collect/registrants/{form}',
            res.url
        )
        if not (payment_m or free_m):
            raise AssoConnectClientException(
                f"MEMBERSHIP: unable to finalize sale {sale_id} for "
                f" transaction {transaction_id} for '{email}'."
            )

        logger.info(
            f"MEMBERSHIP: creation completed for '{email}', "
            f"transaction {transaction_id}"
        )

        if free_m:
            logger.info(
                f"PAYMENT: AssoConnect payment NOT REQUIRED for transaction "
                f"{transaction_id} for '{email}'"
            )
            return {
                'transaction_id': transaction_id,
                'payment_required': False
            }

        if payment_m:
            if transaction_id != payment_m.groups()[0]:
                raise AssoConnectClientException(
                    'PAYMENT: payment_id -- transaction_id INCONSISTENCY!'
                )
            logger.info(
                f"PAYMENT: AssoConnect payment REQUIRED for transaction "
                f"{transaction_id} for '{email}'"
            )
            return {
                'transaction_id': transaction_id,
                'payment_required': True
            }

    def _pay_membership_assoconnect(user_id, transaction_id,
                                    target_bank='', target_cash=''):
        if use_assoconnect_payment:
            logger.info(
                f''
            )
        date_str = datetime.datetime.today().strftime("%Y%m%d000000")
        res = post_form(
            self.s, res.url,
            data={
                'BuyPackerAccountingRecordDate': date_str,
                'BuyPackerAccountingEntryPaymentMethod': 'b',
                'BuyPackerAccountingEntryOptionCheckName': '',
                'BuyPackerAccountingEntryOptionCheckBank': '',
                'BuyPackerAccountingEntryOptionCheckNumber': '',
                'BuyPackerAccountingEntryOptionOtherCheckNumber': '',
                'accountingSepaNumber': 0,
                'accountingSepaFrequency': '',
                'BuyPackerAccountingEntryAccountingBankAccountId': target_bank,
                'BuyPackerAccountingEntryPettyCashId': target_cash,
                'BuyPackerAccountingEntryComment': '',
                'BuyPackerAccountingEntryUserIdHelper': '',
                'BuyPackerAccountingEntryUserId': user_id,
            }
        )

        assert re.match(r'https://stapa.assoconnect.com/collect/registrants/[0-9]+\?messageSuccess.+', res.url)
        re.match(r'https://stapa.assoconnect.com/organization/contacts/[0-9]+\?messageSuccess.+')
