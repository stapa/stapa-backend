import time
import datetime
import json
from functools import wraps
from pathlib import Path
from uuid import UUID
from urllib.parse import urljoin

from dateutil.relativedelta import relativedelta
import greenlet
import requests
from flask import (Flask, render_template, session, request, redirect, url_for,
                   current_app, Response)
from flask_cors import CORS
from PIL import Image
from pyzbar import pyzbar
from pypdf import PdfReader
from flask_session import Session
from werkzeug.exceptions import HTTPException, BadRequest, NotFound, Forbidden
from itsdangerous.url_safe import URLSafeSerializer
from itsdangerous.exc import BadSignature

from sqlalchemy import select, and_, func
from sqlalchemy.orm import scoped_session

from . import logging
from .config import settings
from .db.database import engine, SessionLocal
from .db.models import (User,
                        PasteurPosition,
                        StripeCheckout,
                        SubscriptionOption,
                        Subscription,
                        StripePaymentIntent,
                        WalletTransaction,
                        Task,
                        Poll,
                        PollMemberVoted,
                        PollVote)
from .auth.session import AuthSession
from .auth import azure
from .utils.validator import validate, ValidationError
from .stripe import StripeClient
from .email import send_welcome
from . import dolibarr


app = Flask(__name__)

CORS(
    app,
    supports_credentials=True,
    origins=[
        f"^{r}$".replace('.', r'\.').replace('*', r'[^/?]*?')
        for r in settings.AUTHORIZED_CORS_ORIGINS
    ]
)

app.db = scoped_session(
    SessionLocal,
    scopefunc=greenlet.getcurrent
)
app.config['MAX_CONTENT_LENGTH'] = 32 * 1000 * 1000
app.config['SESSION_COOKIE_SECURE'] = True
# SameSite: None - required by native, where Origin: https://localhost
app.config['SESSION_COOKIE_SAMESITE'] = 'None'

auth = AuthSession(app)


def azure_user(session, create=False):
    oid = session['user']['oid']
    # graph_token = azure.get_token(session)
    # if not graph_token:
    #     return False

    query = select(User).where(
        User.pasteur_azure_ad_oid == UUID(oid)
    )
    user = app.db.execute(query).scalar_one_or_none()

    if not user:
        session['user'] = {}
        app.logger.error(
            f'Alien session: oid not in DB. Session detected and deleted.')
        raise BadRequest('Alien session!')
    return user


@app.teardown_appcontext
def remove_scoped_session(*args, **kwargs):
    app.db.remove()


@app.errorhandler(HTTPException)
def handle_exception(e):
    app.logger.info(
        f'{e.code} {e.name}: {e.description}'
    )
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


@app.errorhandler(ValidationError)
def handle_validation_error(e):
    app.logger.info(
        f'400 Bad Request: {e.message}'
    )
    response = Response()
    response.status_code = 400
    response.data = json.dumps({
        "code": 400,
        "name": "Bad Request",
        "description": str(e.message)
    })
    response.content_type = 'application/json'
    return response


### -------------------------------
### *        API: COMMON          *
### -------------------------------
@app.route('/debug/session')
def debug_session():
    return dict(session)


@app.route("/me")
@auth.login_required
def info():
    user = azure_user(session)
    return {
        'oid': session['user'].get('oid'),
        'name': session['user'].get('name'),
        'username': session['user'].get('preferred_username'),
        'has_card': bool([s for s in user.subscriptions if s.active]),
        'balance': user.wallet_balance
    }


@app.route('/membercard')
@auth.login_required
def membercard():
    user = azure_user(session)
    active = [s for s in user.subscriptions if s.active]
    if not active:
        return {}, 404

    active.sort(key=lambda x: x.remaining_days, reverse=True)
    until = active[0].valid_to
    return {
        'id': user.id,
        'pronouns': user.pronouns,
        'display_name': user.display_name,
        'valid_to': until.isoformat()
    }


@app.route("/members", methods=['GET'])
# TODO: @auth.bureau_required
@auth.login_required
def member_search():
    if 'search' in request.args:
        words = request.args['search'].split(' ')
        query = select(User).where(and_(
            *[User.display_name.ilike(f"%{word}%") for word in words]
        ))
    elif 'qrcode' in request.args:
        qrcode = request.args['qrcode']
        query = select(User).where(User.pasteur_matricule == qrcode)
    else:
        raise BadRequest("Invalid search arguments")

    results = app.db.execute(query).scalars()
    return [{
        'id': user.id,
        'display_name': user.display_name,
        'pronouns': user.pronouns,
        'balance': user.wallet_balance
    } for user in results]


### ----------------------------------------
### *     API: MEMBERSHIP ONBOARDING       *
### ----------------------------------------
@app.route("/onboarding/auth")
@auth.login_required
def onboarding_auth():
    user = azure_user(session)
    return {
        'given_name': user.given_name,
        'family_name': user.family_name,
        'display_name': user.display_name,
        'pasteur_email_address': user.pasteur_email_address,
        'status': user.status
    }


@app.route("/onboarding/contact")
@auth.login_required
def onboarding_contact():
    user = azure_user(session)
    return {
        'pronouns': user.pronouns,
        'birthdate': user.birthdate.isoformat() if user.birthdate else None,
        'phone_number': user.phone_number,
        'email_address': user.email_address,
        'news_consent': user.news_consent,
        'status': user.status
    }


@app.route("/onboarding/contact", methods=['PUT'])
@auth.login_required
def onboarding_contact_update():
    schema = {
        'type': 'object',
        'properties': {
            'pronouns': {'type': ['string', 'null']},
            'birthdate': {'type': 'string', 'format': 'date'},
            'phone_number': {'type': ['string', 'null']},
            'email_address': {'type': 'string', 'format': 'email'},
            'news_consent': {'type': 'boolean'},
            'joinDL': {'type': 'boolean'},
            'joinTeams': {'type': 'boolean'}
        },
        'additionalProperties': False,
        'required': ['birthdate', 'email_address', 'news_consent']
    }
    data = request.get_json()
    validate(data, schema)
    user = azure_user(session)
    user.pronouns = request.json['pronouns']
    user.birthdate = datetime.date.fromisoformat(request.json['birthdate'])
    user.phone_number = request.json.get('phone_number', None)
    user.email_address = request.json['email_address']
    user.news_consent = request.json['news_consent']
    if request.json.get('joinDL', False):
        app.db.add(Task(task={'action': 'join_stapa-all', 'user_id': user.id}))
    if request.json.get('joinTeams', False):
        app.db.add(Task(task={'action': 'join_teams', 'user_id': user.id}))
    app.db.commit()
    dolibarr.update_member(user)

    current_app.logger.info(
        'ONBOARDING: Contact form submitted by user, DB updated.',
        extra={'user_data': data}
    )
    return {}, 204


@app.route("/onboarding/badge")
@auth.login_required
def onboarding_badge():
    user = azure_user(session)
    return {
        'matricule': user.pasteur_matricule,
        'pasteur_position_id': user.pasteur_position_id,
        'pasteur_position_other': user.pasteur_position_other
    }


@app.route("/onboarding/badge/positions")
@auth.login_required
def list_positions():
    positions = app.db.scalars(select(PasteurPosition)).all()
    return [{'id': p.id, 'description': p.description}
            for p in positions]


@app.route("/onboarding/badge", methods=['PUT'])
@auth.login_required
def onboarding_badge_update():
    schema = {
        'type': 'object',
        'properties': {
            'matricule': {'type': 'string'},
            'pasteur_position_id': {'type': 'number'},
            'pasteur_position_other': {'type': 'string'}
        },
        'additionalProperties': False,
        'oneOf': [
            {'required': ['matricule', 'pasteur_position_id']},
            {'required': ['matricule', 'pasteur_position_other']}
        ]
    }
    data = request.get_json()
    validate(data, schema)

    user = azure_user(session)
    user.pasteur_matricule = request.json['matricule']

    # fetch position to ensure it exists
    pos_id = request.json.get('pasteur_position_id', None)
    if pos_id:
        res = app.db.execute(select(PasteurPosition).where(PasteurPosition.id == pos_id)).scalar_one_or_none()
        if not res:
            raise BadRequest(f"Invalid position id: {pos_id}")
        user.pasteur_position_id = request.json.get('pasteur_position_id', None)
        user.pasteur_position_other = None
    else:
        user.pasteur_position_id = None
        user.pasteur_position_other = request.json.get('pasteur_position_other', None)
    app.db.commit()
    dolibarr.update_member(user)
    current_app.logger.info(
        'ONBOARDING: Badge form submitted by user, DB updated.',
        extra={'user_data': data}
    )
    return {}, 204


@app.route("/onboarding/badge/scan", methods=['POST'])
@auth.login_required
def onboarding_badge_scan():
    if 'badge' not in request.files:
        raise BadRequest("Missing file: 'badge'")

    if request.files['badge'].mimetype == 'application/pdf':
        reader = PdfReader(request.files['badge'])
        qrcodes = [
            qr
            for page in reader.pages
            for image in page.images
            for qr in pyzbar.decode(image)
        ]
    else:
        image = Image.open(request.files['badge'])
        qrcodes = pyzbar.decode(image)

    if len(qrcodes) != 1:
        return {'matricule': None}

    qrcode = qrcodes[0].data.decode('utf-8')
    if not qrcode.isdigit():
        # TODO: better invalid badge handling
        return {'matricule': None}

    return {'matricule': qrcode}


@app.route("/onboarding/payment")
@auth.login_required
def onboarding_payment():
    stripe = StripeClient()
    user = azure_user(session)

    remaining_days = (
        max([s.remaining_days for s in user.subscriptions])
        if user.subscriptions else 0
    )

    pending = [
        {'url': co.url,
         'subscription_option': co.subscription_option.to_dict()}
        for co in user.stripe_checkouts
        if co.subscription_option and co.status == 'pending'
    ]

    available = [
        o for o in app.db.execute(
            select(SubscriptionOption).where(SubscriptionOption.selectable)
        ).scalars()
    ]

    return {
        'confirmed': [e.to_dict() for e in user.subscriptions],
        'available': ([e.to_dict() for e in available]
                      if remaining_days < 30 else []),
        'pending_checkouts': pending,
    }


@app.route("/onboarding/payment/subscription_option/<sub_option_id>",
           methods=['POST'])
@auth.login_required
def onboarding_payment_create(sub_option_id):
    stripe = StripeClient()
    user = azure_user(session)

    remaining_days = (
        max([s.remaining_days for s in user.subscriptions])
        if user.subscriptions else 0
    )
    if remaining_days > 30:
        raise BadRequest(
            'ONBOARDING PAY: Trying to pay for a new membership with one'
            'already valid and not expiring in the next 30 days.'
        )

    pending = [
        co
        for co in user.stripe_checkouts
        if co.subscription_option and co.status == 'pending'
    ]
    if pending:
        raise BadRequest(
            'ONBOARDING PAY: Attempt to generate new checkout while one '
            'pending.'
        )

    query = select(SubscriptionOption).where(
        SubscriptionOption.id == sub_option_id)
    subscription_option = app.db.execute(query).scalar_one_or_none()
    if not subscription_option:
        raise BadRequest(
            f'ONBOARDING PAY: Required subscription option {sub_option_id} '
            f'does not exists.'
        )
    if not subscription_option.selectable:
        raise BadRequest(
            f'ONBOARDING PAY: Required subscription option {sub_option_id} '
            f'is not selectable.'
        )

    if subscription_option.type == 'absolute':
        valid_from = subscription_option.valid_from
        valid_to = subscription_option.valid_to
    elif subscription_option.type == 'moving':
        valid_from = datetime.datetime.today()
        valid_to = valid_from + relativedelta(
            months=subscription_option.valid_for_months)

    sub_type = subscription_option.type

    metadata = {
        'dol_member_id': dolibarr.update_member(user),
        'dol_subscription_start': int(valid_from.timestamp()),
        'dol_subscription_end': int(valid_to.timestamp())
    }

    if subscription_option.price == 0:
        # Generate subscription immediately
        app.db.add(Subscription(
            user=user,
            valid_from=valid_from,
            valid_to=valid_to
        ))
        app.db.commit()
        app.logger.info(
            f"SUBSCRIPTION: Free membership created for "
            f"'{user.pasteur_email_address}' for subscription option "
            f"{subscription_option.id}."
        )
        send_welcome(user)

        return {
            'payment_link': urljoin(settings.FRONTEND_BASE, '/onboarding?payment=success')
        }

    return {
        'payment_link': stripe.create_subscription_checkout(
            user,
            subscription_option,
            urljoin(settings.FRONTEND_BASE, '/onboarding?payment=success'),
            metadata=metadata
        )
    }


@app.route("/onboarding/payment/verify")
@auth.login_required
def onboarding_payment_verify():
    user = azure_user(session)
    stripe = StripeClient()
    pending_checkouts = [
        co for co in user.stripe_checkouts
        if co.status == 'pending'
    ]
    if len(pending_checkouts) > 1:
        app.logger.warning(
            f"ONBOARDING PAY: MULTIPLE pending checkout for "
            f"{user.pasteur_email_address}'. This should not happen!"
        )

        # Provide validation result only for last created checkout
        pending_checkouts.sort(key=lambda x: x.creation_date, reverse=True)

    if not pending_checkouts:
        raise NotFound('No pending payments.')

    for checkout in pending_checkouts:
        result = stripe.verify_subscription_checkout(checkout)
    return result

### -------------------------------
### *        API: POLLS           *
### -------------------------------
@app.route("/polls/<int:poll_id>/token")
def poll_check(poll_id):
    ser = URLSafeSerializer(settings.APP_SECRET_KEY)
    if not 'auth_token' in request.args:
        raise BadRequest("Invalid request, missing auth token")
    
    try:
        token_data = ser.loads(request.args['auth_token'])
    except BadSignature:
        raise BadRequest("Invalid request, bad auth token")

    if token_data.get('poll_id') != poll_id:
        raise BadRequest("Invalid request, poll id mismatch")

    user = app.db.execute(select(User).where(User.id == token_data.get('user_id'))).scalar_one_or_none()
    if not user:
        raise BadRequest("Invalid request, user id does not exist")
    poll = app.db.execute(select(Poll).where(Poll.id == token_data.get('poll_id'))).scalar_one_or_none()
    if not poll:
        raise BadRequest("Invalid request, poll id does not exist")
    has_voted = app.db.execute(select(PollMemberVoted).where(
        (PollMemberVoted.user == user)
        &(PollMemberVoted.poll == poll))).scalar_one_or_none() is not None
    
    return {
        'open': poll.open,
        'voted': has_voted,
        'auth_token': request.args['auth_token']
    }


@app.route("/polls/<int:poll_id>/token", methods=['POST'])
def poll_post(poll_id):
    ser = URLSafeSerializer(settings.APP_SECRET_KEY)
    if not 'auth_token' in request.args:
        raise BadRequest("Invalid request, missing auth token")
    
    token_data = ser.loads(request.args['auth_token'])
    if token_data.get('poll_id') != poll_id:
        raise BadRequest("Invalid request, poll id mismatch")

    # TODO: move poll schema definition in database
    schema = {
        'type': 'object',
        'properties': {
            'director': {'type': ['integer', 'null']},
            'vicedirector': {'type': ['integer', 'null']},
            'secretary': {'type': ['integer', 'null']},
            'treasurer': {'type': ['integer', 'null']},
            'ipin': {'type': ['integer', 'null']}
        },
        'additionalProperties': False,
        'required': ['director', 'vicedirector', 'secretary', 'treasurer', 'ipin']
    }
    vote_data = request.get_json()
    validate(vote_data, schema)

    user = app.db.execute(select(User).where(User.id == token_data.get('user_id')).with_for_update()).scalar_one_or_none()
    if not user:
        raise BadRequest("Invalid request, user id does not exist")
    poll = app.db.execute(select(Poll).where(Poll.id == token_data.get('poll_id'))).scalar_one_or_none()
    if not poll:
        raise BadRequest("Invalid request, poll id does not exist")
    has_voted = app.db.execute(select(PollMemberVoted).where(
        (PollMemberVoted.user == user)
        &(PollMemberVoted.poll == poll))).scalar_one_or_none() is not None
    
    if has_voted:
        app.logger.warning(f"User '{user.pasteur_email_address}' tried to vote twice for poll {poll.id}")
        raise Forbidden("Already voted for this poll!")
    
    if not poll.open:
        app.logger.info(f"User '{user.pasteur_email_address}' tried to vote for closed poll {poll.id}")
        raise Forbidden("Poll is closed!")

    app.db.add_all([
        PollMemberVoted(user=user, poll=poll),
        PollVote(poll=poll, vote=vote_data)
    ])
    app.db.commit()
    app.logger.info(f"User '{user.pasteur_email_address}' vote registered for poll {poll.id}")
    return {}, 204

@app.route("/polls/<int:poll_id>")
def poll_stats(poll_id):
    poll = app.db.execute(select(Poll).where(Poll.id == poll_id)).scalar_one_or_none()
    if not poll:
        raise NotFound("poll id does not exist")

    def create_results():
        results = {}
        for vote in app.db.scalars(select(PollVote).where(PollVote.poll==poll)).all():
            for key, value in vote.vote.items():
                if value is None:
                    value = 0
                if key not in results:
                    results[key] = {}
                if value not in results[key]:
                    results[key][value] = 0
                results[key][value] += 1
        return results

    valid_votes = len(app.db.scalars(select(PollMemberVoted).where(PollMemberVoted.poll==poll)).all())
    if poll.open:
        return {
            'status': 'open',
            'open_at': poll.not_before,
            'close_at': poll.not_after,
            'remaining_seconds': int((poll.not_after - datetime.datetime.utcnow()).total_seconds()) if poll.not_after else None,
            'valid_votes': valid_votes
        }
    elif datetime.datetime.utcnow() >= poll.not_after:
        return {
            'status': 'completed',
            'open_at': poll.not_before,
            'close_at': poll.not_after,
            'remaining_seconds': 0,
            'valid_votes': valid_votes,
            'results': create_results()
        }



    


### -------------------------------
### *        API: STRIPE          *
### -------------------------------
@app.route("/stripeCallback", methods=['POST'])
def stripe_callback():
    stripe = StripeClient()
    stripe.webhook(request)
    return "", 200


@app.route("/terminal/stripe/token", methods=['POST'])
# TODO: @auth.bureau_required
@auth.login_required
def terminal_stripe_token():
    user = azure_user(session)
    stripe = StripeClient()
    app.logger.info(f"Creating a Stripe Terminal token, operator: "
                    f" '{user.pasteur_email_address}'.")
    return stripe.terminal_get_token()


@app.route("/terminal/stripe/payment", methods=['POST'])
# TODO: @auth.bureau_required
@auth.login_required
def terminal_stripe_create_payment():
    operator = azure_user(session)
    schema = {
        'type': 'object',
        'properties': {
            'member_id': {'type': 'integer'},
            'amount': {'type': 'integer'},
            'reason': {'type': 'string', 'enum': ['topup']}
        },
        'additionalProperties': False,
        'required': ['member_id', 'amount', 'reason']
    }
    data = request.get_json()
    validate(data, schema)
    stripe = StripeClient()
    query = select(User).where(User.id == data['member_id'])
    member = app.db.execute(query).scalar_one_or_none()
    if not member:
        raise BadRequest('Member ID does not exist')
    return stripe.create_payment_intent(member, operator, data['amount'])


@app.route("/terminal/stripe/payment/<payment_id>", methods=['GET'])
# TODO: @auth.bureau_required
@auth.login_required
def terminal_stripe_verify_payment(payment_id):
    query = select(StripePaymentIntent).where(
        StripePaymentIntent.id == payment_id)
    payment_intent = app.db.execute(query).scalar_one_or_none()
    if not payment_intent:
        raise NotFound()
    stripe = StripeClient()
    return stripe.verify_topup_payment(payment_intent)


### -------------------------------
### *     API: STAPA WALLET       *
### -------------------------------
@app.route("/wallet/self/free_drink", methods=['GET'])
@auth.login_required
def self_free_drink_check():
    user = azure_user(session)
    # check if already had free beer
    since = datetime.datetime.utcnow() - datetime.timedelta(days=1)
    query = select(WalletTransaction).where(and_(
        WalletTransaction.user_id == user.id,
        WalletTransaction.creation_date > since,
        WalletTransaction.type == 'withdrawal',
        WalletTransaction.amount == 0
    ))
    free_beers_last_24h = app.db.execute(query).scalar_one_or_none()
    return {
        'eligible': not free_beers_last_24h
    }

@app.route("/wallet/self/free_drink", methods=['POST'])
@auth.login_required
def self_free_drink_redeem():
    user = azure_user(session)
    app.db.refresh(user, with_for_update=True)
    since = datetime.datetime.utcnow() - datetime.timedelta(days=1)
    query = select(WalletTransaction).where(and_(
        WalletTransaction.user_id == user.id,
        WalletTransaction.creation_date > since,
        WalletTransaction.type == 'withdrawal',
        WalletTransaction.amount == 0
    ))
    free_beers_last_24h = app.db.execute(query).scalar_one_or_none()
    if not free_beers_last_24h:
        app.db.add(WalletTransaction(user=user, type='withdrawal',
                                     reason='free_drink',
                                     amount=0))
        app.db.commit()
        app.logger.info(f"TX: EXECUTED SELF FREE BEER'"
                        f"for user '{user.pasteur_email_address}'")
        return {'executed': True}
    else:
        app.logger.info(f"TX: REJECTED SELF FREE BEER'"
                        f"for user '{user.pasteur_email_address}'")
        return {'executed': False}

@app.route("/wallet/transaction", methods=['POST'])
# TODO: @auth.bureau_required
@auth.login_required
def execute_transaction():
    schema = {
        'type': 'object',
        'properties': {
            'member_id': {'type': 'integer'},
            'cash_in': {'type': 'integer'},
            'lines': {
                'type': 'array',
                'items': {
                    'type': 'object',
                    'properties': {
                        'type': {'type': 'string', 'enum': ['beer']},
                        'quantity': {'type': 'integer'}
                    },
                    'additionalProperties': False,
                    'required': ['type', 'quantity'],
                }
            },
        },
        'additionalProperties': False,
        'required': ['member_id', 'cash_in', 'lines']
    }
    data = request.get_json()
    validate(data, schema)
    if not data['lines']:
        return BadRequest('No lines, nothing to do.')
    operator = azure_user(session)
    query = select(User).where(User.id == data['member_id'])
    user = app.db.execute(query.with_for_update()).scalar_one_or_none()
    if not user:
        return BadRequest('Member ID does not exist')
    requested_beers = sum([line['quantity'] for line in data['lines']
                           if line['type'] == 'beer'])
    balance_before = user.wallet_balance
    # check if already had free beer
    since = datetime.datetime.utcnow() - datetime.timedelta(days=1)
    query = select(WalletTransaction).where(and_(
        WalletTransaction.user_id == data['member_id'],
        WalletTransaction.creation_date > since,
        WalletTransaction.type == 'withdrawal',
        WalletTransaction.amount == 0
    ))
    free_beers_last_24h = app.db.execute(query).scalar_one_or_none()
    app.logger.info(f"TX: User '{user.pasteur_email_address}' "
                    f"({balance_before=}, {free_beers_last_24h=}) "
                    f" requested {requested_beers} beers.")
    tx_paid_beers = (requested_beers if free_beers_last_24h
                     else requested_beers - 1)
    tx_free_beers = 0 if free_beers_last_24h else 1
    balance_after = (balance_before
                     + data['cash_in']
                     - tx_paid_beers * 100)
    lines = []
    if data['cash_in'] and balance_after >= 0:
        app.db.add(WalletTransaction(user=user, type='deposit',
                                     reason='cash_topup',
                                     operator=operator,
                                     amount=data['cash_in']))
    if tx_paid_beers:
        lines.append({'type': 'beer',
                      'quantity': tx_paid_beers,
                      'unit_price': 100})
        if balance_after >= 0:
            app.db.add(WalletTransaction(user=user, type='withdrawal',
                                         reason='paid_drinks',
                                         operator=operator,
                                         amount=tx_paid_beers * 100))
    if tx_free_beers:
        lines.append({'type': 'free_beer',
                      'quantity': tx_free_beers, 'unit_price': 0})
        if balance_after >= 0:
            app.db.add(WalletTransaction(user=user, type='withdrawal',
                                         reason='free_drink',
                                         operator=operator,
                                         amount=0))
    app.db.commit()
    app.logger.info(f"TX: {'EXECUTED' if balance_after >=0 else 'REJECTED'}'"
                    f"for user '{user.pasteur_email_address}'"
                    f"({balance_after=}, {tx_paid_beers=}, {tx_free_beers=}),"
                    f" operator '{operator.pasteur_email_address}'.")
    return {
        'executed': balance_after >= 0,
        'balance_before': balance_before,
        'balance_after': balance_after,
        'lines': lines
    }


@app.route('/statistics', methods=['GET'])
# TODO: @auth.bureau_required
@auth.login_required
def statistics():
    since = datetime.datetime.utcnow() - datetime.timedelta(days=1)
    query_free = select(func.count()).select_from(WalletTransaction).where(and_(
        WalletTransaction.creation_date > since,
        WalletTransaction.type == 'withdrawal',
        WalletTransaction.amount == 0
    ))
    number_free = app.db.execute(query_free).scalar()
    query_paid = select(func.sum(WalletTransaction.amount)).where(and_(
        WalletTransaction.creation_date > since,
        WalletTransaction.type == 'withdrawal',
        WalletTransaction.amount != 0
    ))
    earnings = app.db.execute(query_paid).scalar()
    earnings = earnings if earnings else 0
    number_paid = earnings / 100
    return {
        'number_free': number_free,
        'number_paid': int(number_paid),
        'earnings': earnings
    }
