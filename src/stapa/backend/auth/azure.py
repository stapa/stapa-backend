import msal

from ..config import settings


def logout_url():
    return (settings.AZURE_AUTHORITY
            + '/oauth2/v2.0/logout')


def login_flow(redirect_uri, state=None):
    msal_app = msal.ConfidentialClientApplication(
        settings.AZURE_CLIENT_ID,
        authority=settings.AZURE_AUTHORITY,
        client_credential=settings.AZURE_CLIENT_SECRET
    )

    return msal_app.initiate_auth_code_flow(
        settings.AZURE_SCOPE,
        state=state,
        redirect_uri=redirect_uri
    )


def get_msal_app(token_cache):
    return msal.ConfidentialClientApplication(
        settings.AZURE_CLIENT_ID,
        authority=settings.AZURE_AUTHORITY,
        client_credential=settings.AZURE_CLIENT_SECRET,
        token_cache=token_cache
    )


def login_handle_callback(azure_flow, callback_args, session):
    """Catch MS AD Azure authentication callback"""
    try:
        token_cache = msal.SerializableTokenCache()
        if session.get('token_cache'):
            token_cache.deserialize(session['token_cache'])

        msal_app = get_msal_app(token_cache)
        result = msal_app.acquire_token_by_auth_code_flow(
            azure_flow, callback_args
        )
        session['token_cache'] = token_cache.serialize()
        return result
    except ValueError:  # Usually caused by CSRF
        pass  # Simply ignore them


def get_token(session):
    token_cache = msal.SerializableTokenCache()
    token_cache.deserialize(session['token_cache'])
    msal_app = get_msal_app(token_cache)
    accounts = msal_app.get_accounts()
    if accounts:
        result = msal_app.acquire_token_silent(
            settings.AZURE_SCOPE,
            account=accounts[0]
        )
        if token_cache.has_state_changed:
            session['token_cache'] = token_cache.serialize()
        return result
