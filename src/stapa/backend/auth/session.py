import inspect
from flask import session, request, url_for, redirect, current_app
from functools import wraps
from uuid import UUID

import requests
from flask_session import Session
from flask_classful import FlaskView, route
import werkzeug.exceptions
from sqlalchemy import select

from . import azure
from ..db.models import User
from .native import NativeToken, NativeTokenNotFound
from ..config import settings
from ..utils.validator import validate, ValidationError


class MissingSecretsException(Exception):
    pass


class Unauthorized(werkzeug.exceptions.Unauthorized):
    def __init__(self, message):
        current_app.logger.info(
            f'401 Unauthorized: {message}'
        )
        super().__init__(message)


class BadRequest(werkzeug.exceptions.BadRequest):
    def __init__(self, message):
        current_app.logger.info(
            f'400 Bad Request: {message}'
        )
        super().__init__(message)


class AuthSession:
    def __init__(self, app):
        for conf in ['AZURE_CLIENT_ID', 'AZURE_CLIENT_SECRET']:
            if conf not in settings:
                raise MissingSecretsException(conf)
        app.config.update({
            k: v
            for k, v in settings.to_dict().items()
            if k.startswith('SESSION_')
        })
        Session(app)
        AuthView.register(app)

    @classmethod
    def login_required(cls, f, scope=None):
        @wraps(f)
        def decorator(*args, **kwargs):
            if not cls.authenticated():
                raise Unauthorized("Session not authenticated.")
            return f(*args, **kwargs)
        return decorator

    @classmethod
    def authenticated(cls):
        # TODO: [security] extra checks
        return bool(session.get('user', ''))


class AuthView(FlaskView):
    @route('/login', methods=['POST'])
    def login(self):
        if AuthSession.authenticated():
            raise BadRequest('Already logged in.')

        data = request.get_json()

        schema = {
            'type': 'object',
            'properties': {
                'redirect_url': {
                    'type': 'string',
                    'anyOf': [
                        {
                            'pattern': f"^{r}$".replace('.', r'\.').replace('*', r'[^/?]*?')
                        }
                        for r in settings.AUTHORIZED_REDIRECTS
                    ]
                },
                'native_app': {
                    'type': 'boolean'
                }
            },
            'additionalProperties': False,
            'required': ['redirect_url']
        }
        validate(data, schema)

        redirect_url = data['redirect_url']

        if bool(data.get('native_app')):
            state = NativeToken.from_session(current_app, session).to_state()
        else:
            state = None

        callback_url = url_for('msal_callback', _external=True)
        azure_flow = azure.login_flow(callback_url, state=state)

        session['azure_flow'] = azure_flow
        session['redirect_url'] = redirect_url

        return {
            'login_url': azure_flow['auth_uri'],
        }

    @route('/msalCallback', endpoint='msal_callback')
    def msal_callback(self):
        state = request.args.get('state', '')
        native_tk = NativeToken.from_state(current_app, state)

        used_session = native_tk.session if native_tk else session

        azure_flow = used_session.get('azure_flow')
        redirect_url = used_session.get('redirect_url')

        if not azure_flow or not redirect_url:
            raise BadRequest('Invalid session cookie.')

        result = azure.login_handle_callback(
            azure_flow, request.args, used_session)

        if 'error' in result:
            return {'code': 502,
                    'name': 'Bad Gateway',
                    'description': f'Microsoft auth failed: {result}',
                    'msal_result': result}, 502

        used_session['user'] = result.get('id_token_claims')
        self._sync_db_azure(used_session)
        return redirect(redirect_url)

    def _sync_db_azure(self, session):
        oid = session['user']['oid']
        graph_token = azure.get_token(session)
        if not graph_token:
            return False

        query = select(User).where(
            User.pasteur_azure_ad_oid == UUID(oid)
        )

        user = current_app.db.execute(query).scalar_one_or_none()
        if not user:
            res = requests.get(
                "https://graph.microsoft.com/v1.0/me",
                headers={
                    "Authorization": "Bearer " + graph_token['access_token']
                }
            )
            me = res.json()
            user = User(
                given_name=me['givenName'],
                family_name=me['surname'],
                display_name=me['displayName'],
                pasteur_email_address=me['mail'],
                pasteur_azure_ad_oid=UUID(me['id']),
                pasteur_azure_ad_upn=me['userPrincipalName']
            )
            current_app.db.add(user)
            current_app.db.commit()
            current_app.logger.info(
                'LOGIN: Data synced from azure on first login',
                extra={'user_data': me}
            )

    @route('/logout', methods=['POST'])
    def logout(self):
        session.clear()
        # after_url = settings.URL_FRONTEND
        azure_logout_url = azure.logout_url()
        return {'logout_url': azure_logout_url}
