import os
import json
from secrets import token_urlsafe
from pathlib import Path

from ..config import settings
from ..utils.session_editor import SessionEditor


class NativeTokenNotFound(Exception):
    pass


class NativeToken:
    """
    Helper to have MSAL auth code flow authentication on native apps

    In Native apps the authentication flow is started on a new browser
    window, with a completely different session and cookie context.

    After the authentication the browser is redirected to the backend
    callback endpoint.
    The callback endpoint should be aware of the session state in the
    context of the native app, to read and write session variables.

    We pass a nonce to the `state` http query parameter of the
    /authorized MSAL endpoint to reference the session of the user.
    """
    def __init__(self, app, token, sid=None):
        self.app = app
        self.token = token
        self.sid = sid

        self.editor = SessionEditor.from_app(app)

        if sid:
            self.editor.save_token(self.token, self.sid)
        else:
            self.sid = self.editor.load_token(self.token)
            if not self.sid:
                raise NativeTokenNotFound()

    @classmethod
    def from_session(cls, app, session):
        sid = session.sid
        # TODO: handle signer
        token = token_urlsafe(32)
        return cls(app, token, sid)

    @classmethod
    def from_state(cls, app, state):
        if state.startswith('nativeToken:'):
            return cls(app, state.split('nativeToken:')[1])
        else:
            return None

    def to_state(self):
        return 'nativeToken:' + self.token

    @property
    def session(self):
        return self.editor.session(self.sid)
