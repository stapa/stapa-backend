import datetime
import enum
from typing import List, Optional, Literal
from uuid import UUID

from dateutil.relativedelta import relativedelta
from sqlalchemy import ForeignKey, select, func
from sqlalchemy.sql import expression, and_, case
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.types import DateTime
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.dialects.postgresql import JSONB

from .database import Base


UserStatus = Literal['incomplete', 'active', 'disabled']
CheckoutStatus = Literal['pending', 'paid', 'failed']
PaymentIntentStatus = Literal['requires_payment_method',
                              'requires_action',
                              'processing',
                              'succeeded',
                              'canceled']
TaskStatus = Literal['new', 'executed', 'failed']
SubscriptionType = Literal['absolute', 'moving']


class utcnow(expression.FunctionElement):
    type = DateTime()
    inherit_cache = True


@compiles(utcnow, 'postgresql')
def pg_utcnow(element, compiler, **kw):
    return "TIMEZONE('utc', CURRENT_TIMESTAMP)"


@compiles(utcnow, 'mssql')
def ms_utcnow(element, compiler, **kw):
    return "GETUTCDATE()"


@compiles(utcnow, 'sqlite')
def sqlite_utcnow(element, compiler, **kw):
    return "DATETIME()"


class User(Base):
    __tablename__ = "users"

    id: Mapped[int] = mapped_column(primary_key=True)
    given_name: Mapped[Optional[str]]
    family_name: Mapped[Optional[str]]
    display_name: Mapped[Optional[str]]
    birthdate: Mapped[Optional[datetime.date]]
    pronouns: Mapped[Optional[str]]

    phone_number: Mapped[Optional[str]]
    email_address: Mapped[Optional[str]]
    news_consent: Mapped[Optional[bool]]

    pasteur_email_address: Mapped[Optional[str]]
    pasteur_azure_ad_oid: Mapped[Optional[UUID]] = mapped_column(index=True,
                                                                 unique=True)
    pasteur_azure_ad_upn: Mapped[Optional[str]]
    pasteur_matricule: Mapped[Optional[str]] = mapped_column(index=True)
    pasteur_badge_nfc_uid: Mapped[Optional[str]] = mapped_column(index=True)

    pasteur_position_id: Mapped[Optional[int]] = mapped_column(ForeignKey("pasteur_positions.id"))
    pasteur_position: Mapped[Optional['PasteurPosition']] = relationship()
    pasteur_position_other: Mapped[Optional[str]]

    status: Mapped[UserStatus] = mapped_column(server_default='incomplete')
    subscriptions: Mapped[List["Subscription"]] = relationship(
        back_populates="user",
        cascade="all, delete-orphan"
    )

    stripe_customer_id: Mapped[Optional[str]]
    stripe_checkouts: Mapped[List['StripeCheckout']] = relationship(
        back_populates="user")

    wallet_transactions: Mapped[List['WalletTransaction']] = relationship(
        back_populates="user", foreign_keys="WalletTransaction.user_id")

    @hybrid_property
    def wallet_balance(self):
        sign = {
            'withdrawal': -1,
            'deposit': 1
        }
        return sum([txn.amount * sign[txn.type]
                    for txn in self.wallet_transactions])

    @wallet_balance.expression
    def wallet_balance(cls):
        return select(
            func.sum(case(
                {
                    'withdrawal': -WalletTransaction.amount,
                    'deposit': WalletTransaction.amount
                },
                value=WalletTransaction.type)
            )).where(WalletTransaction.user_id == cls.id).as_scalar()


class PasteurPosition(Base):
    __tablename__ = "pasteur_positions"

    id: Mapped[int] = mapped_column(primary_key=True)
    description: Mapped[str]


class SubscriptionOption(Base):
    __tablename__ = "subscription_options"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]

    price: Mapped[int]
    stripe_price_id: Mapped[Optional[str]]

    selectable_from: Mapped[datetime.datetime]
    selectable_to: Mapped[datetime.datetime]

    @hybrid_property
    def selectable(self):
        now = datetime.datetime.utcnow()
        return (self.selectable_from < now
                and self.selectable_to > now)

    @selectable.expression
    def selectable(cls):
        return and_(cls.selectable_from < utcnow(),
                    cls.selectable_to > utcnow())

    type: Mapped[SubscriptionType]
    valid_from: Mapped[Optional[datetime.datetime]]
    valid_to: Mapped[Optional[datetime.datetime]]
    valid_for_months: Mapped[Optional[int]]

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'price': self.price,
            'valid_from': (
                self.valid_from.isoformat() if self.valid_from
                else None
            ),
            'valid_to': (
                self.valid_to.isoformat() if self.valid_to
                else (datetime.datetime.utcnow()
                      + relativedelta(months=self.valid_for_months))
            ),
            'valid_for_months': self.valid_for_months
        }


class Subscription(Base):
    __tablename__ = "subscriptions"
    id: Mapped[int] = mapped_column(primary_key=True)

    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    user: Mapped['User'] = relationship(back_populates="subscriptions")

    valid_from: Mapped[datetime.datetime]
    valid_to: Mapped[datetime.datetime]

    @property
    def remaining_days(self):
        now = datetime.datetime.utcnow()
        return (self.valid_to - now).days

    @hybrid_property
    def active(self):
        now = datetime.datetime.utcnow()
        return (self.valid_from < now
                and self.valid_to > now)

    @active.expression
    def active(cls):
        return and_(cls.valid_from < utcnow(),
                    cls.valid_to > utcnow())

    creation_date: Mapped[datetime.datetime] = mapped_column(
        server_default=utcnow()
    )

    def to_dict(self):
        return {
            'id': self.id,
            'active': self.active,
            'valid_from': self.valid_from.isoformat(),
            'valid_to': self.valid_to.isoformat(),
            'remaining_days': self.remaining_days
        }


class StripeCheckout(Base):
    __tablename__ = "stripe_checkouts"
    id: Mapped[str] = mapped_column(primary_key=True)

    url: Mapped[str]

    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    user: Mapped['User'] = relationship(back_populates="stripe_checkouts")

    status: Mapped[CheckoutStatus] = mapped_column(server_default='pending')

    subscription_option_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("subscription_options.id"))
    subscription_option: Mapped[Optional['SubscriptionOption']] = relationship()

    wallet_tokens: Mapped[Optional[int]]

    creation_date: Mapped[datetime.datetime] = mapped_column(
        server_default=utcnow()
    )


class StripePaymentIntent(Base):
    __tablename__ = "stripe_payment_intents"
    id: Mapped[str] = mapped_column(primary_key=True)
    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    user: Mapped['User'] = relationship(foreign_keys=user_id)

    status: Mapped[PaymentIntentStatus] = mapped_column(
        server_default="requires_payment_method")

    amount: Mapped[int]

    creation_date: Mapped[datetime.datetime] = mapped_column(
        server_default=utcnow()
    )

    operator_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    operator: Mapped['User'] = relationship(foreign_keys=operator_id)


class WalletTransaction(Base):
    __tablename__ = "wallet_transactions"

    id: Mapped[int] = mapped_column(primary_key=True)

    type: Mapped[Literal['withdrawal', 'deposit']]
    reason: Mapped[Literal['card_topup', 'cash_topup', 'free_drink', 'paid_drinks']]
    amount: Mapped[int]

    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    user: Mapped["User"] = relationship(back_populates="wallet_transactions", foreign_keys=user_id)

    operator_id: Mapped[Optional[int]] = mapped_column(ForeignKey("users.id"))
    operator: Mapped[Optional["User"]] = relationship(foreign_keys=operator_id)

    stripe_payment_intent_id: Mapped[Optional[str]] = mapped_column(ForeignKey("stripe_payment_intents.id"))
    stripe_payment_intent: Mapped[Optional['StripePaymentIntent']] = relationship()

    creation_date: Mapped[datetime.datetime] = mapped_column(
        server_default=utcnow()
    )


class Task(Base):
    __tablename__ = "tasks"
    id: Mapped[int] = mapped_column(primary_key=True)

    status: Mapped[TaskStatus] = mapped_column(server_default='new')
    task: Mapped[dict] = mapped_column(type_=JSONB)

    not_before: Mapped[Optional[datetime.datetime]]
    not_after: Mapped[Optional[datetime.datetime]]

    @hybrid_property
    def executable(self):
        now = datetime.datetime.utcnow()
        return not (
            (self.not_before and now < self.not_before)
            or (self.not_after and now > self.not_after)
            or (self.status == 'executed')
        )

    @executable.expression
    def executable(cls):
        return ~(
            ((cls.not_before != None) & (utcnow() < cls.not_before))
            | ((cls.not_after != None) & (utcnow() > cls.not_after))
            | (cls.status == 'executed')
        )

    creation_date: Mapped[datetime.datetime] = mapped_column(
        server_default=utcnow()
    )

class Poll(Base):
    __tablename__ = "polls"
    id: Mapped[int] = mapped_column(primary_key=True)
    description: Mapped[str]

    not_before: Mapped[Optional[datetime.datetime]]
    not_after: Mapped[Optional[datetime.datetime]]

    @hybrid_property
    def open(self):
        now = datetime.datetime.utcnow()
        return not (
            (self.not_before and now < self.not_before)
            or (self.not_after and now > self.not_after)
        )

    @open.expression
    def open(cls):
        return ~(
            ((cls.not_before != None) & (utcnow() < cls.not_before))
            | ((cls.not_after != None) & (utcnow() > cls.not_after))
        )


    creation_date: Mapped[datetime.datetime] = mapped_column(
        server_default=utcnow()
    )

class PollMemberVoted(Base):
    __tablename__ = "poll_member_voted"

    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"), primary_key=True)
    poll_id: Mapped[int] = mapped_column(ForeignKey("polls.id"), primary_key=True)

    user: Mapped["User"] = relationship()
    poll: Mapped["Poll"] = relationship()


class PollVote(Base):
    __tablename__ = "poll_votes"
    id: Mapped[int] = mapped_column(primary_key=True)

    poll_id: Mapped[int] = mapped_column(ForeignKey("polls.id"))
    poll: Mapped["Poll"] = relationship()

    vote: Mapped[dict] = mapped_column(type_=JSONB)
