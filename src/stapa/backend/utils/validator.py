from jsonschema import Draft202012Validator, FormatChecker
from jsonschema.exceptions import ValidationError


def validate(instance, schema):
    validator = Draft202012Validator(schema, format_checker=FormatChecker())
    return validator.validate(instance)
