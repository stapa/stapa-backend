import os

from flask_session.sessions import (
    FileSystemSessionInterface,
    RedisSessionInterface,
    total_seconds
)


class UnsupportedSessionInterface(Exception):
    pass


class SessionNotFound(Exception):
    pass


class SessionEditor:
    SUPPORTED_SESSIONS = [
        FileSystemSessionInterface,
        RedisSessionInterface
    ]

    @classmethod
    def from_app(cls, app):
        session_type = type(app.session_interface)
        config = app.config.copy()
        config.setdefault('SESSION_TYPE', 'null')
        config.setdefault('SESSION_PERMANENT', True)
        config.setdefault('SESSION_KEY_PREFIX', 'session:')
        config.setdefault('SESSION_REDIS', None)
        config.setdefault('SESSION_FILE_DIR',
                          os.path.join(os.getcwd(), 'flask_session'))
        config.setdefault('SESSION_FILE_THRESHOLD', 500)
        config.setdefault('SESSION_FILE_MODE', 384)

        if session_type == FileSystemSessionInterface:
            return FileSystemSessionEditor(
                config['SESSION_FILE_DIR'], config['SESSION_FILE_THRESHOLD'],
                config['SESSION_FILE_MODE'], config['SESSION_KEY_PREFIX'],
                total_seconds(app.permanent_session_lifetime))
        else:
            raise UnsupportedSessionInterface(session_type)


# TODO: create RedisSessionEditor


class FileSystemSessionEditor(SessionEditor):
    def __init__(self, cache_dir, threshold, mode, key_prefix, timeout):
        from cachelib.file import FileSystemCache
        self.cache = FileSystemCache(cache_dir, threshold=threshold, mode=mode)
        self.key_prefix = key_prefix
        self.timeout = timeout

    def save_token(self, token, sid):
        self.cache.set(self.key_prefix + token, sid, self.timeout)

    def load_token(self, token):
        sid = self.cache.get(self.key_prefix + token)
        if not sid:
            return None
        self.cache.delete(self.key_prefix + token)
        return sid

    def session(self, sid):
        return SessionDict(sid, self.read_session, self.save_session)

    def read_session(self, sid):
        data = self.cache.get(self.key_prefix + sid)
        if not data:
            return {}
        return data

    def save_session(self, sid, data):
        self.cache.set(self.key_prefix + sid, data)


class SessionDict:
    def __init__(self, sid, reader, writer):
        self.sid = sid
        self.reader = reader
        self.writer = writer

    def get(self, k):
        return self.reader(self.sid).get(k)

    def __contains__(self, k):
        return self.reader(self.sid).__contains__(k)

    def __getitem__(self, k):
        return self.reader(self.sid)[k]

    def __setitem__(self, k, v):
        data = self.reader(self.sid)
        data[k] = v
        self.writer(self.sid, data)
