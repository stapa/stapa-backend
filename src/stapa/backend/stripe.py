import requests
import datetime

from dateutil.relativedelta import relativedelta
from flask import current_app, has_app_context

from .config import settings
from .db.models import (
    User,
    StripeCheckout,
    SubscriptionOption,
    Subscription,
    StripePaymentIntent,
    WalletTransaction
)

from .email import send_welcome

import logging
logger = logging.getLogger(__name__)


class StripeException(Exception):
    def __init__(self, message):
        logging.error(message)
        super().__init__(message)


class StripeClient:
    ENDPOINT = 'https://api.stripe.com/v1'

    def __init__(self):
        if has_app_context():
            self.db = current_app.db
        else:
            from .db.database import SessionLocal
            self.db = SessionLocal()

        self.s = requests.Session()
        self.s.headers.update(
            {'Authorization': 'Bearer ' + settings.STRIPE_SECRET_KEY})

    def check_customer(self, user: User):
        if not user.pasteur_email_address:
            raise StripeException(
                'User does not possess a @pasteur.fr e-mail address, needed to'
                ' create Customer.'
            )
        if not user.stripe_customer_id:
            res = self.s.post(
                self.ENDPOINT + '/customers',
                data={'name': user.display_name,
                      'email': user.pasteur_email_address})
            if res.status_code != 200:
                raise StripeException(res.json())
            data = res.json()
            user.stripe_customer_id = data['id']
            self.db.commit()
            logger.info(
                f"USER: New customer created for "
                f"'{user.pasteur_email_address}': {user.stripe_customer_id}"
            )
        else:
            logger.info(
                f"USER: Customer already exists for "
                f"'{user.pasteur_email_address}': {user.stripe_customer_id}"
            )
        return user.stripe_customer_id

    def create_subscription_checkout(self,
                                     user: User,
                                     subscription_option: SubscriptionOption,
                                     success_url,
                                     metadata={}):
        customer = self.check_customer(user)
        checkout_data = {
            'success_url': success_url,
            'customer': customer,
            'line_items[0][price]': subscription_option.stripe_price_id,
            'line_items[0][quantity]': 1,
            'mode': 'payment'
        }
        for k, v in metadata.items():
            checkout_data[f'payment_intent_data[metadata][{k}]'] = v

        res = self.s.post(
            self.ENDPOINT + '/checkout/sessions',
            data=checkout_data
        )
        if res.status_code != 200:
            raise StripeException(res.json())
        data = res.json()
        logger.info(
            f"PAYMENT: Checkout session created for "
            f"'{user.pasteur_email_address}' for subscription option "
            f"{subscription_option.id}: {data['id']}"
        )
        checkout = StripeCheckout(
            id=data['id'],
            subscription_option=subscription_option,
            user=user,
            url=data['url']
        )
        current_app.db.add(checkout)
        current_app.db.commit()

        return checkout.url

    def verify_subscription_checkout(self,
                                     subscription_checkout: StripeCheckout):
        res = self.s.get(
            self.ENDPOINT + '/checkout/sessions/' + subscription_checkout.id
        )
        if res.status_code != 200:
            raise StripeException(res.json())
        data = res.json()

        email = subscription_checkout.user.pasteur_email_address
        old_status = subscription_checkout.status

        if data['status'] == 'expired':
            subscription_checkout.status = 'failed'
            current_app.db.commit()

        if data['payment_status'] == 'paid':
            self.db.refresh(subscription_checkout, with_for_update=True)
            if subscription_checkout.status == 'pending':
                subscription_checkout.status = 'paid'
                self.generate_subscription(subscription_checkout)
            else:
                self.db.commit()

        logger.info(
            f"PAYMENT: Checkout session {subscription_checkout.id} "
            f"status for '{email}': "
            f"[{old_status}] -> [{subscription_checkout.status}] "
            f"checkout_status={data['status']}, "
            f"payment_status={data['payment_status']}")

        return {'checkout_status': data['status'],
                'payment_status': data['payment_status']}

    def generate_subscription(self, subscription_checkout):
        sub_option = subscription_checkout.subscription_option
        if sub_option.type == 'absolute':
            valid_from = sub_option.valid_from
            valid_to = sub_option.valid_to
        elif sub_option.type == 'moving':
            valid_from = datetime.datetime.today()
            valid_to = valid_from + relativedelta(
                months=sub_option.valid_for_months)

        current_app.db.add(Subscription(
            user=subscription_checkout.user,
            valid_from=valid_from,
            valid_to=valid_to
        ))
        # current_app.db.add(SubscriptionHooksQueueElement(
        #     subscription_checkout=subscription_checkout,
        #     subscription=subscription
        # ))
        current_app.db.commit()
        send_welcome(subscription_checkout.user)

    def generate_wallet_transaction(self, payment_intent: StripePaymentIntent):
        logger.info(f'Wallet new deposit transaction: {payment_intent.user}'
                    f' + {payment_intent.amount/100} EUR')
        current_app.db.add(WalletTransaction(
            user=payment_intent.user,
            amount=payment_intent.amount,
            operator_id=payment_intent.operator_id,
            type='deposit',
            reason='card_topup',
            stripe_payment_intent=payment_intent
        ))
        current_app.db.commit()
        # TODO: send_receipt()

    def create_price(self, name, price, statement_descriptor=None):
        res = self.s.post(
            self.ENDPOINT + '/prices',
            data={
                'currency': 'eur',
                'unit_amount': price,
                'product_data[name]': name,
                'product_data[statement_descriptor]': statement_descriptor
            })
        if res.status_code != 200:
            StripeException(f"PRICE: unable to create price for '{name}':  "
                            f"{res.json()}")
        price_id = res.json()['id']
        logger.info(f"PRICE: created for '{name}': {price_id}")
        return price_id

    def create_payment_intent(self,
                              user: User,
                              operator: User,
                              amount: int,
                              metadata={}):
        customer = self.check_customer(user)
        payment_intent_data = {
            'customer': customer,
            'amount': amount,
            'currency': 'eur',
            'payment_method_types[]': 'card_present'
        }
        metadata['operator_id'] = operator.id
        metadata['operator_ad_oid'] = str(operator.pasteur_azure_ad_oid)
        metadata['operator_ad_upn'] = operator.pasteur_azure_ad_upn

        for k, v in metadata.items():
            payment_intent_data[f'metadata[{k}]'] = v

        res = self.s.post(
            self.ENDPOINT + '/payment_intents',
            data=payment_intent_data
        )
        if res.status_code != 200:
            raise StripeException(res.json())
        data = res.json()
        logger.info(
            f"PAYMENT: Payment intent created for "
            f"'{user.pasteur_email_address}' for EUR "
            f"{amount/100} by '{operator.pasteur_email_address}'"
        )
        payment_intent = StripePaymentIntent(
            id=data['id'],
            user=user,
            status=data['status'],
            amount=amount,
            operator=operator
        )
        current_app.db.add(payment_intent)
        current_app.db.commit()

        return data

    def verify_topup_payment(self,
                             payment_intent: StripePaymentIntent):
        res = self.s.get(
            self.ENDPOINT + '/payment_intents/' + payment_intent.id
        )
        if res.status_code != 200:
            raise StripeException(res.json())
        data = res.json()

        old_status = payment_intent.status
        new_status = data['status']

        if new_status != 'succeeded':
            payment_intent.status = new_status
            current_app.db.commit()

        if new_status == 'succeeded':
            self.db.refresh(payment_intent, with_for_update=True)
            if payment_intent.status != 'succeeded':
                payment_intent.status = 'succeeded'
                self.generate_wallet_transaction(payment_intent)
            else:
                self.db.commit()

        logger.info(
            f"PAYMENT: Payment Intent session {payment_intent.id} "
            f"status for '{payment_intent.user.pasteur_email_address}': "
            f"[{old_status}] -> [{payment_intent.status}]"
        )

        return data

    def terminal_get_token(self):
        res = self.s.post(self.ENDPOINT + '/terminal/connection_tokens')
        if res.status_code != 200:
            raise StripeException(res.json())
        return res.json()

    def webhook(self, request):
        data = request.get_json()
        logger.info(f'STRIPE WEBHOOK: {data}')
