import os
import importlib.resources
from pathlib import Path

from dynaconf import Dynaconf


builtin_settings = importlib.resources.files(__package__) / 'settings.toml'


class Stapaconf(Dynaconf):
    def __init__(self):
        super().__init__(
            envvar_prefix='STAPA',
            env_switcher='STAPA_MODE',
            environments=True,
            settings_files=[
                builtin_settings,
                'settings.toml',
                '.secrets.toml'
            ]
        )
        self._load_secrets()

    def _load_secrets(self):
        secrets_file = Path(os.environ.get('STAPA_SECRETS', ''))
        if secrets_file.is_file():
            self.configure(secrets_for_dynaconf=str(secrets_file))

    def reload_secrets(self):
        self._load_secrets()


settings = Stapaconf()
