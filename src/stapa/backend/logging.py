import time
import logging
import json
from copy import copy

import flask.logging
from flask import has_request_context, request, session
from logzio.handler import LogzioHandler

from .config import settings

try:
    import uwsgi
    from uwsgidecorators import postfork
except ImportError:
    def postfork(func):
        func()
        return func


class RequestFormatter(logging.Formatter):
    def format(self, record):
        record = copy(record)
        if has_request_context():
            record.request = json.dumps({
                'url': request.url,
                'remote_addr': request.remote_addr,
                'session_id': session.sid,
                'azure_oid': session.get('user', {}).get('oid'),
                'azure_username': session.get('user', {}).get('preferred_username')
            })
        else:
            record.request = None

        return super().format(record)


class LogzioFormatter(logging.Formatter):
    def __init__(self):
        super().__init__('%(message)s', validate=False)

    def format(self, record):
        record.environment = settings.env_name
        if has_request_context():
            record.request = {
                'url': request.url,
                'remote_addr': request.remote_addr,
                'session_id': session.sid,
                'azure_oid': session.get('user', {}).get('oid'),
                'azure_username': session.get('user', {}).get('preferred_username')
            }
        return super().format(record)


@postfork
def configure_logging():
    log_formatter = RequestFormatter(
        fmt=('%(asctime)-15s %(levelname)-8s [%(name)s] %(message)s'
             ' - Request: %(request)s'
             ' - PID: %(process)d - Thread: %(threadName)s'))
    log_formatter.converter = time.gmtime
    log_formatter.default_msec_format = '%s.%06dZ'
    log_formatter.default_time_format = '%Y-%m-%dT%H:%M:%S'

    log_handler = logging.StreamHandler(flask.logging.wsgi_errors_stream)
    log_handler.setFormatter(log_formatter)

    logzio_handler = LogzioHandler(
        host=settings.OPENSEARCH_HOST,
        port=settings.OPENSEARCH_PORT,
        username=settings.OPENSEARCH_USERNAME,
        password=settings.OPENSEARCH_PASSWORD,
        logzio_type='python',
        logs_drain_timeout=5,
    )

    logzio_formatter = LogzioFormatter()
    logzio_handler.setFormatter(logzio_formatter)

    logging.basicConfig(level=logging.INFO,
                        handlers=[
                            log_handler,
                            logzio_handler
                        ])
